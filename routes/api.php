<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::resource('users', 'UserController');*/

Route::namespace('Web')->group(function() {
    Route::get('logout', 'AuthController@apiLogout');
});

Route::namespace('Api')->middleware(['auth:api'])->group(function() {
    Route::prefix('me')->group(function() {
        Route::get('/', 'UserController@me');
        Route::get('runsheets', 'UserController@runsheet');
        Route::get('runsheet/{runsheetId}/leg', 'LegController@getLeg');
        Route::post('leg/{leg}', 'SubmitController@send');
        Route::get('task-leg/{task}', 'TaskController@getTask');

        Route::prefix('task')->group(function() {
            Route::get('current', 'TaskController@current');
            Route::get('completed', 'TaskController@completed');
            Route::get('delayed', 'TaskController@delayed');
        });
    });
});