<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::namespace('Web')->group(function() {
    Route::get('/', ['uses' => 'LandingController@index', 'as' => '/']);

    Route::get('login', 'AuthController@getLogin');
    Route::post('login', 'AuthController@login')->name('login');
    Route::get('logout', 'AuthController@logout')->name('logout');

    Route::middleware(['auth', 'role:superadministrator'])->prefix('admin')->group(function() {
        Route::get('/', 'AdminController@index')->name('admin.index');
        Route::resource('hierarchy', 'HeirarchyController', ['as' => 'admin']);
        Route::get('users/search', 'UserController@searchUser')->name('user.search');
        Route::post('user/bind', 'UserController@bind')->name('user.bind');
        Route::resource('users', 'UserController', ['as' => 'admin']);
        Route::resource('groups', 'GroupController', ['as' => 'admin']);

        Route::get('runsheets/{runsheetId}/leg/{legId}', 'RunsheetController@leg')->name('admin.leg.show');
        Route::post('runsheets/{runsheetId}/leg', 'LegController@store')->name('admin.runsheet.leg.store');
        Route::post('runsheets/{runsheetId}/leg/{legId}', 'LegController@legStore')->name('admin.runsheet.leg.field.store');
        Route::post('runsheets/{runsheetId}/publish', 'RunsheetController@published')->name('admin.runsheet.publish');
        Route::get('runsheets/{runsheet}/preview', 'RunsheetController@preview')->name('admin.runsheet.preview');

        Route::resource('runsheets', 'RunsheetController', ['as' => 'admin', 'except' => [
            'create',
        ]]);

        Route::get('leg/{id}/preview', 'LegController@preview')->name('admin.leg.preview');
        Route::resource('leg', 'LegController', ['as' => 'admin', 'except' => [
            'index',
            'store',
            'create',
            'destroy',
            'show'
        ]]);
    });
});
