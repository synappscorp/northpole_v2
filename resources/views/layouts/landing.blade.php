<!DOCTYPE html>
<html>
	@include('partials.landing-header')
<body>
	@yield('navbar')

	@yield('content')

	@include('partials.landing-scripts')

    @yield('js')
</body>
</html>