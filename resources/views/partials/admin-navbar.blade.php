<nav class="main-nav nav-extended">
    <div class="container nav-wrapper">
        <a href="#" class="brand-logo left"><img src="/img/northpole-logo-landscape-white.png" class="responsive-img" alt="..."></a>
        <ul class="mobile-nav right hide-on-med-and-down">
            <li>
                <a class="dropdown-button" href="#!" data-activates="dropdown1" data-beloworigin="true">
                    {{Auth::user()->username}}
                    <i class="material-icons right">arrow_drop_down</i>
                    <img class="responsive-img circle" src="https://picsum.photos/200/200/" />
                </a>
            </li>
        </ul>
    </div>
    <div class="row nav-content">
        <div class="col l8 s12 offset-l2">
            <ul class="tabs tabs-fixed-width">
                <li class="tab">
                    <a href="{{ url('/admin') }}" class="{!!(Request::is('admin')) ? 'active' : '#' !!}" target="_self">
                        <img src="/img/dashboard.svg" />
                        <span>Dashboard</span>
                    </a>
                </li>
                <li class="tab">
                    <a href="{{ url('/admin/runsheets') }}" class="{!!(Request::is('admin/runsheets')) || (Request::is('admin/runsheets/*')) ? 'active' : '#' !!}" target="_self">
                        <img src="/img/runsheets.svg" />
                        <span>Runsheets</span>
                    </a>
                </li>
                <li class="tab">
                        <a href="{{ url('/admin/hierarchy') }}" class="{!!(Request::is('admin/hierarchy')) ? 'active' : '#' !!}" target="_self">
                        <img src="/img/hierarchy.svg" />
                        <span>Hierarchy</span>
                    </a>
                </li>
                <li class="tab">
                    <a href="{{ url('/admin/groups') }}" class="{!!(Request::is('admin/groups')) || (Request::is('admin/groups/*')) ? 'active' : '#' !!}" target="_self">
                        <img src="/img/group.svg" />
                        <span>Groups</span>
                    </a>
                </li>
                <li class="tab">
                    <a href="{{ url('/admin/users') }}" class="{!!(Request::is('admin/users')) || (Request::is('admin/users/*')) ? 'active' : '#' !!}" target="_self">
                        <img src="/img/user.svg" />
                        <span>Users</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<nav class="sub-nav">
    <div class="container nav-wrapper">
      <div class="col s12">
        <a href="#!" class="breadcrumb">Northpole</a>
        @if(Request::is('admin/users'))
        <a href="{{ url('/admin/users') }}" class="breadcrumb">Users</a>
        @elseif(Request::is('admin/users/*'))
        <a href="{{ url('/admin/users') }}" class="breadcrumb">Users</a>
        <a href="#!" class="breadcrumb">User Information</a>
        {{-- @elseif(Request::is('admin/forms') || Request::is('admin/forms/*'))
        <a href="#!" class="breadcrumb">Run Sheets</a> --}}
        @elseif(Request::is('admin/groups') || Request::is('admin/groups/*'))
        <a href="#!" class="breadcrumb">Groups</a>
        @elseif(Request::is('admin/hierarchy'))
        <a href="{{ url('/admin/hierarchy') }}" class="breadcrumb">Hierarchy</a>
        @elseif(Request::is('admin/runsheets') || Request::is('admin/runsheets/*'))
        <a href="{{ url('/admin/runsheets') }}" class="breadcrumb">Runsheets</a>
        @else
        <a href="#!" class="breadcrumb">Dashboard</a>
        @endif
      </div>
    </div>
</nav>

<ul id="dropdown1" class="dropdown-content">
  <li><a href="{{ route('logout') }}">Logout</a></li>
  <li class="divider"></li>
</ul>