@if ($data->hasPages())
    <ul class="pagination">
        {{-- Previous Page Link --}}
        @if ($data->onFirstPage())
            <li class="disabled"><span>Previous</span></li>
        @else
            <li><a href="{{ $data->previousPageUrl() }}" rel="prev">Previous</a></li>
        @endif

        {{-- Next Page Link --}}
        @if ($data->hasMorePages())
            <li><a href="{{ $data->nextPageUrl() }}" rel="next">Next</a></li>
        @else
            <li class="disabled"><span>Next</span></li>
        @endif
    </ul>
@endif