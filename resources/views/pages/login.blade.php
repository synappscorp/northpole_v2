@extends('layouts.landing')

@section('custom-style')
    <style type="text/css">
        body, html,form {
            height: 100%;
        }
        form {
            display: flex;
            justify-content: center;
            align-items: center;
        }
        .card-panel i {
            display: block;
        }
        .login-fields {
            width: 400px;
        }
    </style>
@endsection

@section('content')
<div class="row login-page">
    <form method="POST">
        {{ csrf_field() }}
        <div>
            <center class="row">
                <img src="/img/northpole-latest.png" class="img-fluid rounded" alt="...">
            </center>
            <div class="login-fields row">
                @if ($errors->has('email'))
                    <div class="card-panel red center-align white-text">
                        <i class="small material-icons">warning</i>
                        <span>{{ $errors->first('email') }}</span>
                    </div>
                @endif
                <div class="clearfix">
                    <div class="input-field col s12">
                        <input id="email" class="validate" type="text" name="email" value="{{ old('email') }}" required="required" autofocus />
                        <label class="active" for="email" data-error="Email is required.">Email</label>
                    </div>
                </div>
                <div class="clearfix">
                    <div class="input-field col s12">
                        <input id="password" class="validate" type="password" name="password" required="required" />
                        <label for="password" data-error="Password is required.">Password</label>
                    </div>
                </div>
                <div class="clearfix">
                    <center class="input-field col s12">
                        <button type="submit" class="col s12 btn btn-large waves-effect waves-light">Login</button>
                    </center>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection

@section('js')

@endsection