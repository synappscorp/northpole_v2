@extends('layouts.admin')

@section('custom-style')
	<link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
@endsection

@section('content')
	<div class="container">
		<div class="row">
			<div class="col s12 m6">
				<h6 class="dboard-title">
					<i class="material-icons">pie_chart</i>
					<span>Current Stats</span>
				</h6>
				<div class="sheet-chart">
					<div class="input-field viewtype">
					    <select>
					      <option value="mv" selected>Monthly View</option>
					      <option value="yv">Yearly View</option>
					    </select>
				  	</div>
					<div class="input-field sub-viewtype">
					    <select>
					      <option value="nov2017" selected>November 2017</option>
					      <option value="dec2017">December 2017</option>
					    </select>
				  	</div>
				</div>
				<div id="chartdiv" max-width="460px;"></div>
			</div>
			<div class="col s12 m6">
				<h6 class="dboard-title">
					<i class="material-icons">insert_drive_file</i>
					<span>Recent Published Runsheets</span>
				</h6>
				<table class="bordered modified task-table sheet-table">
					<tr>
						<th>RUNSHEET</th>
						<th>PREVIEW</th>
					</tr>
                    @forelse($runsheet as $r)
                    <tr class="task">
                    	<td>
                            <span class="name">{{$r->name}}</span>
                            <span class="sched">{{$r->created_at->diffForHumans()}}</span>
                        </td>
                        <td>
                        	<a href="#" style="display: inline-block;width: 25px;"><img src="/img/view.svg"/></a>
                        </td>
                    </tr>
                    @empty
                    <tr>
                    	<td colspan="2">
                    		No Runsheet Found
                        </td>
                    </tr>
                    @endforelse
                </table>
			</div>
		</div>
	</div>
@endsection

@section('addonjs')
	<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
	<script src="https://www.amcharts.com/lib/3/pie.js"></script>
	<script src="https://www.amcharts.com/lib/3/themes/light.js"></script>
	<script>
		var chart = AmCharts.makeChart( "chartdiv", {
		  "type": "pie",
		  "theme": "light",
		  "marginTop": 100,
		  "marginBottom": 0,
		  "marginLeft": -50,
		  "legend":{
		   	"position":"right",
		    "marginLeft":0,
		    "markerType":"circle",
		    "autoMargins":false,
		    "valueText":false
		  },
		  "dataProvider": [ {
		    "title": "Completed",
		    "value": 10,
		    "color": "#44b72a"
		  }, {
		    "title": "Queued",
		    "value": 20,
		    "color": "#27ace2"
		  }, {
		    "title": "Late",
		    "value": 5,
		    "color": "#f64346"
		  } ],
		  "titleField": "title",
		  "valueField": "value",
		  "colorField": "color",
		  "labelRadius": 5,

		  "radius": "35%",
		  "innerRadius": "65%",
		  "labelText": "[[title]]",
		  "responsive": true
		 //  "allLabels": [
			// 	{
			// 		"text": "250",
			// 		"bold": true,
			// 		"size": 20,
			// 		"x":133,
			// 		"y":230
			// 	},
			// 	{
			// 		"text": "Runsheets",
			// 		"size": 14,
			// 		"color": "#7a7a7a",
			// 		"x":115,
			// 		"y":255
			// 	}
			// ]
		} );

		$(document).ready(function(e) {
			$('select').material_select();
    		setInterval(function(){ $("a[title='JavaScript charts']").hide(); }, 10);
		});
	</script>
@endsection