@extends('layouts.admin')

@section('content')
    @if ($errors->has('message'))
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong>Success</strong> Successfully Added
        </div>
    @endif

    <div class="container">
        <div class="row">
            <h4 class="col l2 s6">
                User
                <img src="/img/search.svg" class="searchIcon"/>
            </h4>
            <h4 class="col l6 s6 push-l4 right-align">
                <a href="#addNew" class="modal-trigger waves-effect waves-light btn btn-medium primary-bg add-user">+ New User</a>
                <a href="#importUsers" class="modal-trigger waves-effect waves-light btn btn-medium green-bg">Import via Excel</a>
            </h4>
        </div>
        <div class="row">
            <table class="table-responsive modified">
                <thead>
                    <tr>
                        <th max-width="320px">Name</th>
                        <th>Group</th>
                        <th>Email</th>
                        <th class="center-align" max-width="250px">Actions</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($data as $user)
                    <tr>
                        <td>
                            @if ($user->account_image == '')
                                <div class="profile-image">{{$user->name_initials}}</div>
                            @else
                                <img src="{{ $user->account_image }}" style="float:left;width: 40px;" />
                            @endif
                            <div class="infos">
                                <h6 class="name">{{ $user->first_name . ' ' . $user->last_name }}</h6>
                                <span class="user_type">{{ $user->user_type }}</span>
                            </div>
                        </td>
                        <td>
                            @foreach ($user->groups as $group)
                                <div class="chip">
                                    {{ $group->name }}
                                </div>
                            @endforeach
                        </td>
                        <td>{{ $user->email }}</td>
                        <td class="center-align">
                            <a href="{{route('admin.users.show', ['id' => $user->id])}}" class="btn-floating tooltipped no-shadow dark-gray-bg actionBtn" data-position="top" data-delay="50" data-tooltip="VIEW">
                                <i class="material-icons">
                                    <img class="actionSvg" src="/img/view.svg"/>
                                </i>
                            </a>
                            <button class="btn-floating tooltipped no-shadow dark-gray-bg actionBtn" data-position="top" data-delay="50" data-tooltip="EDIT">
                                <i class="material-icons">
                                    <img class="actionSvg" src="/img/edit.svg"/>
                                </i>
                            </button>
                            <a href="#deleteUser" class="modal-trigger btn-floating tooltipped no-shadow dark-gray-bg actionBtn deleteUserBtn" data-position="top" data-id="{{$user->id}}" data-delay="50" data-tooltip="DELETE">
                                <i class="material-icons">
                                    <img class="actionSvg" src="/img/trash.svg"/>
                                </i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <form action="{{ route('admin.users.store') }}" method="POST" role="form" enctype="multipart/form-data" id="addNew" class="modal" style="padding-bottom: 20px">
        {{ csrf_field() }}
        @if(!$groups)
            <div class="modal-content">
                <h4>Cannot add user</h4>
                <p>You have an empty groups. Fill out atleast one to create user.</p>
            </div>
            <div class="modal-footer">
                <a href="{{ url('/admin/groups') }}" class="waves-effect waves-light btn btn-medium primary-bg">Add Group</a>
            </div>
        @else
            <div class="part-1">
                <div class="modal-content">
                    <h4>Personal Details</h4>
                    <div class="row">
                        <div class="input-field col s12">
                            <input name="first_name" id="first_name" type="text" class="validate">
                            <label for="first_name">First Name</label>
                        </div>
                        <div class="input-field col s12">
                          <input name="middle_name" id="sfx_name" type="text" class="validate" value="">
                          <label for="sfx_name">Middle Name</label>
                        </div>
                        <div class="input-field col s12">
                          <input name="last_name" id="last_name" type="text" class="validate">
                          <label for="last_name">Last Name</label>
                        </div>
                        <div class="input-field col s12">
                          <input name="phone_number" id="c_num" type="text" class="validate">
                          <label for="c_num">Contact Number</label>
                        </div>
                        <div class="input-field col s12">
                          <input name="email" id="email" type="email" class="validate">
                          <label for="email">Email</label>
                        </div>
                        <div class="input-field col s12">
                          <input name="address" id="address" type="text" class="validate">
                          <label for="address">Address</label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="input-field col s12">
                            <a id="addNewNext" href="#" class="waves-effect waves-light btn btn-medium primary-bg">Next</a>
                            <a href="#" class="add-cancel modal-action modal-close waves-effect waves-light btn btn-medium red lighten-1">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="part-2">
                <div class="modal-content">
                    <h4>User Account Details</h4>
                    <div class="row">
                        <div class="input-field col s12">
                            <input name="username" id="username" type="text" class="validate">
                            <label for="username">Username</label>
                        </div>
                        <div class="input-field col s8">
                            <select name="groups[]" id="dept_group" multiple>
                                <option value="" disabled selected>Choose your option</option>
                                @foreach($groups as $group)
                                    <option value="{{ $group->id }}">{{ $group->name }}</option>
                                @endforeach
                            </select>
                            <label for="dept_group">Department / Group</label>
                        </div>
                        <div class="input-field col s4">
                            <select name="role" id="role">
                                @foreach($roles as $role)
                                    <option value="{{ $role->id }}">{{ $role->name }}</option>
                                @endforeach
                            </select>
                            <label for="role">Role</label>
                        </div>
                        <div class="input-field col s12">
                            <input id="new_dept_group" type="text" class="validate">
                            <label for="new_dept_group">or add a new department or group</label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer row">
                    <div class="input-field col s6 center-align">
                        <a id="addNewBack" href="#" class="waves-effect waves-green btn btn-medium grey">Back</a>
                    </div>
                    <div class="input-field col s6 center-align">
                        <button type="submit" id="btn-add" href="#" class="modal-action modal-close waves-effect waves-blue-grey btn btn-medium primary-bg">Add User</button>
                        <a href="#" class="add-cancel modal-action modal-close waves-effect waves-light btn btn-medium red lighten-1">Cancel</a>
                    </div>
                </div>
            </div>
        @endif
    </form>

    <div id="importUsers" class="modal">
        <div class="modal-content">
            <h4>Import Users</h4>
            <div class="file-field input-field">
                <div class="btn">
                    <span>Excel File</span>
                    <input type="file" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel">
                </div>
                <div class="file-path-wrapper">
                    <input class="file-path validate" type="text" placeholder="Upload one or more files">
                </div>
            </div>
        </div>
        <div class="modal-footer">
          <a id="btn-delete" href="#!" class="modal-action modal-close waves-effect waves-red btn-flat">Delete</a>
          <a href="#!" class="modal-action modal-close waves-effect btn-flat">Cancel</a>
        </div>
    </div>

    <div id="deleteUser" class="modal">
        <div class="modal-content">
          <h4>Delete this user?</h4>
        </div>
        <div class="modal-footer">
          <a id="btn-delete" href="#!" class="modal-action modal-close waves-effect waves-red btn-flat">Delete</a>
          <a href="#!" class="modal-action modal-close waves-effect btn-flat">Cancel</a>
        </div>
    </div>
@endsection

@section('addonjs')
    <script>
        $(document).ready(function(){
            var token = '{{ csrf_token()}}';

            /* Initializations */
            $('.modal').modal({
                dismissible: false
            });
            $('select').material_select();

            // Get Modal Width
            var modalWidth = $('#addNew').width();
            $('#addNewNext').click(function(){
                $("#addNew .part-1").animate({width: 0+'px' }).hide();
                $("#addNew .part-2").animate({width: modalWidth+'px' }).show();
            });
            $('#addNewBack').click(function(){
                $("#addNew .part-1").animate({width: modalWidth - 7 +'px' }).show();
                $("#addNew .part-2").animate({width: 0+'px' }).hide();
            });
            $('a.add-cancel').click(function() {
                $("#addNew .part-2").animate({width: 0+'px' }).hide();
                $("#addNew .part-1").animate({width: modalWidth - 7 +'px' }).show();
            });

            $('.deleteUserBtn').click(function() {
                var user_id = $(this).data('id');
                $('#deleteUser').data("id",user_id);
            });

            $('#deleteUser #btn-delete').click(function() {
                $.ajax({
                    url: '/admin/users/' + $('#deleteUser').data('id'),
                    type: 'POST',
                    data: {
                        _method : 'DELETE',
                        _token : token
                    },
                    success: function(data) {
                        var toastContent = $('<span>User Successfully Deleted!</span>');
                        Materialize.toast(toastContent, 2000, 'red',function(){location.reload()});
                    },
                    error: function(error) {
                        console.log(error);
                    }
                });
            });
        });
    </script>
@endsection