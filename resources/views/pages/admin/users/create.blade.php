<form action="{{ route('users.store') }}" method="POST" role="form" enctype="multipart/form-data">
    {{ csrf_field() }}

    <input type="file" name="image">

    <div class="form-group">
        <label for="">First Name</label>
        <input name="first_name" type="text" class="form-control" placeholder="First Name" required>
    </div>

    <div class="form-group">
        <label for="">Middle Name</label>
        <input name="middle_name" type="text" class="form-control" placeholder="Middle Name">
    </div>

    <div class="form-group">
        <label for="">Last Name</label>
        <input name="last_name" type="text" class="form-control" placeholder="Last Name" required>
    </div>

    <div class="form-group">
        <label for="">Phone Number</label>
        <input name="phone_number" type="text" class="form-control" placeholder="Phone Number" required>
    </div>

    <div class="form-group">
        <label for="">Email</label>
        <input name="email" type="email" class="form-control" placeholder="Email" required>
    </div>

    <div class="form-group">
        <label for="">Address</label>
        <textarea name="address" type="text" class="form-control" required></textarea>
    </div>

    <div class="form-group">
        <label for="">Username</label>
        <input name="username" type="text" class="form-control" placeholder="Username" required>
    </div>

    <div class="form-group">
        <label for="">Department/Group</label>
        <select name="group" id="input" class="form-control" required>
            @foreach($groups as $group)
                <option value="{{ $group->id }}">{{ $group->name }}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group">
        <label for="">Role</label>
        <select name="role" id="input" class="form-control" required>
            @foreach($roles as $role)
                <option value="{{ $role->id }}">{{ $role->display_name }}</option>
            @endforeach
        </select>
    </div>

    <button type="submit" class="btn btn-primary">Submit</button>
</form>