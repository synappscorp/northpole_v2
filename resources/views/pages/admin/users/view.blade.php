@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row">
        <h4 class="col s12" style="margin-bottom: 0;">{{ $user['full_name'] }} <br/></h4>
        <h6 class="col s12" style="color: #78909c;">{{ $user['role']}}</h6>
    </div>
</div>

<nav class="profile-nav row">
    <div class="container">
        <ul class="tabs">
            <li class="tab"><a class="active" href="#infos">INFORMATION</a></li>
            <li class="tab"><a href="#tasks">TASKS</a></li>
        </ul>
    </div>
</nav> 
<div class="row">
    <div class="container">
        <div id="infos">
            <div class="col s7">
                <div class="profile-image-container">
                    @if ($user['account_image'] == '')
                        <div class="profile-image">{{$user['name_initials']}}</div>
                    @else
                        <img src="{{ $user['account_image'] }}" />
                    @endif
                    <div id="editImage" class="waves-effect chip">
                        <img src="/img/edit.svg"/>
                    </div>
                </div>

                <table class="bordered">
                    <tr>
                        <td class="title" width="200px">Complete Name</td>
                        <td>{{ $user['full_name'] }}</td>
                    </tr>
                    <tr>
                        <td class="title">Email</td>
                        <td>{{ $user['email'] }}</td>
                    </tr>
                    <tr>
                        <td class="title">Contact Number</td>
                        <td>{{ $user['phone_number'] }}</td>
                    </tr>
                    <tr>
                        <td class="title">Position</td>
                        <td>{{ $user['role'] }}</td>
                    </tr>
                    <tr>
                        <td class="title">Address</td>
                        <td>{{ $user['address'] }}</td>
                    </tr>
                </table>
            </div>
            <div class="col s4 push-s1">
                <div class="groupings">
                    <h5>Group / Department</h5>
                    <div class="chip">
                        Administrator
                        <i class="close material-icons">close</i>
                    </div>
                    <div id="addGroup" class="waves-effect chip">
                        <i class="material-icons">add</i>
                    </div>
                </div>
                <div class="daily-tasks">
                    <h5>Daily Task List</h5>
                    <span>1 of 3 tasks completed</span>
                    <div class="progress green lighten-3">
                        <div class="determinate green darken-1" style="width: 30%"></div>
                    </div>
                    <table class="task-table centered">
                        <tbody>
                            <tr class="task">
                                <td>
                                    <span class="indicator late"></span>
                                </td>
                                <td>Eclair</td>
                            </tr>
                            <tr class="task">
                                <td>
                                    <span class="indicator done">
                                        <i class="material-icons">check</i>
                                    </span>
                                </td>
                                <td>
                                    <i class="status">Late for 2 days</i>
                                    <h6>Task Name Here</h6>
                                    <span class="date">Completed 3 Hours ago</span>
                                </td>
                            </tr>
                            <tr class="task">
                                <td>
                                    <span class="indicator pending"></span>
                                </td>
                                <td>Eclair</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div id="tasks" class="col s12">
            <div class="col s12">
                <div class="daily-tasks">
                    <div class="col s5">
                        <h5>Daily Task List</h5>
                        <span>1 of 3 tasks completed</span>
                        <div class="progress green lighten-3">
                            <div class="determinate green darken-1" style="width: 30%"></div>
                        </div>
                    </div>
                    <table class="col s12 centered modified task-table">
                        <thead>
                            <tr>
                                <th style="min-width: 200px;">TASK</th>
                                <th>GROUP</th>
                                <th>STATUS</th>
                                <th>PRECEDENT</th>
                                <th>SUBSEQUENT</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="task">
                                <td>
                                    <span class="name">Task name here</span>
                                    <span class="sched">Scheduled for 22 Nov 2017</span>
                                </td>
                                <td>Eclair</td>
                                <td>
                                    <span class="indicator late"></span>
                                    <span class="status">Late for 2 days</span>
                                </td>
                                <td>Eclair</td>
                                <td>Eclair</td>
                            </tr>
                            <tr class="task">
                                <td>
                                    <span class="name">Task name here</span>
                                    <span class="sched">Scheduled for 22 Nov 2017</span>
                                </td>
                                <td>
                                    <h6>Task Name Here</h6>
                                </td>
                                <td>
                                    <span class="indicator done">
                                        <i class="material-icons">check</i>
                                    </span>
                                </td>
                                <td>Eclair</td>
                                <td>Eclair</td>
                            </tr>
                            <tr class="task">
                                <td>Eclair</td>
                                <td>Eclair</td>
                                <td>
                                    <span class="indicator pending"></span>
                                </td>
                                <td>Eclair</td>
                                <td>Eclair</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection