<!DOCTYPE html>
<html>
    <head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <meta name="csrf-token" content="{{ csrf_token() }}">
	    <title>NorthpoleTech | Leg</title>
	    <link rel="shortcut icon" href="/img/favicon.ico" />
	    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
	    <!-- Styles -->
	    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
	    <style type="text/css">
	    	#previewLeg{
	    		width: 80%;
	    	}
	    	#previewLeg .leg-content-inner{
	    		pointer-events: none;
	    	}
	    </style>
	</head>
	<body>
		<div class="row leg-view">
	      	<div class="col s3 sidebar">
	    		<a href="{{ route('admin.runsheets.show', ['runsheet' => $leg->runsheet_id]) }}" class="btn-back">
	    			<i class="fa fa-long-arrow-left"></i>
	    			<span>Go Back</span>
	    		</a>
			    <div class="col s12 navtabs">
			      <ul class="tabs tabs-fixed-width">
			        <li class="tab col s3"><a class="active" href="#leg-fields">FIELDS</a></li>
			        <li class="tab col s3"><a href="#leg-setting">SETTINGS</a></li>
			      </ul>
			    </div>
			    <div id="leg-fields" class="col s12">
			    	<div class="field-leg field-leg-source field-checkbox">
			    		<i class="fa fa-check-square-o"></i>
			    		<span class="field-title">Checkbox</span>
			    	</div>
			    	<div class="field-leg field-leg-source field-date">
			    		<i class="fa fa-calendar-o"></i>
			    		<span class="field-title">Date</span>
			    	</div>
			    	<div class="field-leg field-leg-source field-select">
			    		<i class="fa fa-caret-down"></i>
			    		<span class="field-title">Dropdown</span>
			    	</div>
			    	<div class="field-leg field-leg-source field-multiline">
			    		<i class="fa fa-align-justify"></i>
			    		<span class="field-title">Multiple Line</span>
			    	</div>
			    	<div class="field-leg field-leg-source field-notes">
			    		<i class="fa fa-sticky-note"></i>
			    		<span class="field-title">Notes</span>
			    	</div>
			    	<div class="field-leg field-leg-source field-number">
			    		<i class="fa fa-hashtag"></i>
			    		<span class="field-title">Number</span>
			    	</div>
			    	<div class="field-leg field-leg-source field-radio">
			    		<i class="fa fa-circle-o"></i>
			    		<span class="field-title">Radio Button</span>
			    	</div>
			    	<div class="field-leg field-leg-source field-signature">
			    		<i class="fa fa-pencil"></i>
			    		<span class="field-title">Signature</span>
			    	</div>
			    	<div class="field-leg field-leg-source field-singleline">
			    		<i class="fa fa-minus"></i>
			    		<span class="field-title">Single Line</span>
			    	</div>
			    	<div class="field-leg field-leg-source field-time">
			    		<i class="fa fa-clock-o"></i>
			    		<span class="field-title">Time</span>
			    	</div>
			    	<div class="field-leg field-leg-source field-upload">
			    		<i class="fa fa-image"></i>
			    		<span class="field-title">Upload Image</span>
			    	</div>
			    </div>
			    <div id="leg-setting" class="col s12">
			    	{{-- Settings --}}
			    </div>
	      	</div>

	      	<div id="leg-main-content" class="col s9">
	      		<div class="row">
		        	<div class="col l8 leg-title">
		            	<h4>{{$leg->name}}</h4>
		            	<p>Updated {{$leg->updated_at->diffForHumans()}}</p>
		            </div>
		            <h4 class="col l4 right-align">
		            	<div class="row">
		            		<div class="col s6">
		               			<a href="#previewLeg" class="modal-trigger waves-effect waves-light btn btn-medium transparent leg-preview">
		               				<img src="/img/view.svg"/>
			               			<span>Preview</span>
			               		</a>
		               		</div>
		            		<div class="col s6">
		            			<a id="saveLegField" href="#" class="modal-trigger waves-effect waves-light btn btn-medium primary-bg leg-save">
		            				<i class="material-icons">check</i>
		            				<span>Save Leg</span>
		            			</a>
		            		</div>
		            	</div>
		            </h4>
	            </div>
		        <div class="row">
		        	<div id="leg-content" class="col s12 leg-content-inner">
		        		<div class="card leg loading-leg-field center-align">
		        			<div class="preloader-wrapper big active">
							    <div class="spinner-layer spinner-blue-only">
							      	<div class="circle-clipper left">
							        	<div class="circle"></div>
							      	</div>
							      	<div class="gap-patch">
							        	<div class="circle"></div>
							      	</div>
						      		<div class="circle-clipper right">
						        		<div class="circle"></div>
						      		</div>
						    	</div>
						  	</div>
		        			<h6>Loading Legs</h6>
		        		</div>
		        		<div class="card leg empty-leg-field center-align" style="display: none">
		        			<h6>You have no fields</h6>
		        			<p>Drag and Drop fields to give body to your form.</p>
		        			<p>Edit the field by going to the settings tab.</p>
		        		</div>
		        		{{-- Add class 'leg-field-preseted' for edit --}}
		        	</div>
		        </div>
	      	</div>
	    </div>
	    <div id="previewLeg" class="modal">
		    <div class="modal-content leg-content-inner">
		    	<h5>Preview</h5>
		    </div>
		    <div class="modal-footer center-align">
	            <a href="#" class="modal-action modal-close waves-effect waves-blue-grey btn btn-medium green">Close Preview</a>
		    </div>
		</div>
	</body>
	<script type="text/javascript" src="{{ mix('js/app.js') }}"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('.modal').modal();
			$.ajax({
			   	type: 'GET',
			   	url: '/admin/leg/' + {{$leg->id}} + '/preview',
			   	beforeSend: function(){
					$('.empty-leg-field').hide();
					$('.loading-leg-field').show();
			   	},
			   	success: function(data) {
			   		console.log(data.data);
					$('.loading-leg-field').hide();
			     	if(!data.data.fields.length > 0){
					    $('.empty-leg-field').show();
					}else{
					    runsheetEdit(data.data);
					}
			   	},
			   	error: function(data) {
			     	console.log(data);
			   	}
			});
		    
			var token = '{{ csrf_token()}}';
			var url = "{{ route('admin.runsheet.leg.field.store', ['runsheetID' => $leg->runsheet_id, 'legId' => $leg->id]) }}";

			runsheetSave(token,url);
			
			$('.leg-preview').click(function(){
				$('#previewLeg').find('.leg-field-delete').remove();
			});
		});
	</script>
	
</html>