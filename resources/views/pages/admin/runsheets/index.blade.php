@extends('layouts.admin')

@section('custom-style')
	<style type="text/css">
		.runsheet-filter {
			padding: 0px 5px;
			box-shadow: unset !important;
			border: 1px solid #78909c;
		}
		.empty-runsheet {
			display: flex;
			flex-direction: column;
			align-items: center;
			font-size: 1.25em;
			color: #78909c;
		}
		.empty-runsheet img {
			width: 80px;
		}
		.empty-runsheet p {
			font-size: .75em;
			margin: 0;
		}
		.card .right img {
			width: 20px;
			pointer-events: none;
		}
		.chip.pblsh {
			color: white;
			background-color: #43b62e;
			text-transform: uppercase;
		}
		.chip.inprog {
			color: white;
			background-color: #e5c90d;
			text-transform: uppercase;
		}
		.runsheet .card-content {
			padding:30px;
		}
		.runsheet-content {
			margin-top: 30px;
			text-align: center;
		}
		.runsheet-title {
			font-size: 1.25em;
		}
		.runsheet-group {
			color: #26a69a;
		}
		.runsheet-date {
			color: #7a7a7a;
		}
		.runsheet-info {
			color: #7a7a7a;
			margin-top: 30px;
		}
		.card-footer {
			padding: 20px 0;
		}
		.card-footer a {
			color: black;
		}
		.dropdown-content {
			width: initial !important;
		}
		.runsheet-filtered .chip.primary-bg {
			color: white;
		}
		.modal {
			top: 5%;
			max-height: 80%;
		}
		#pubRunsheet,
		#delRunsheet {
			width: 33%;
		}
		#pubRunsheet .modal-footer,
		#delRunsheet .modal-footer {
			text-align: center;
		}
		#prevRunsheet {
			width: 80%;
		}
		#prevRunsheet .modal-content {
			pointer-events: none;
		}
	</style>
@endsection

@section('content')
	<div class="container">
        <div class="row">
            <h4 class="col l2 s6"> Runsheets </h4>
            <h4 class="col l6 s6 push-l4 right-align">
                <a href="#addNewRunsheet" class="modal-trigger waves-effect waves-light btn btn-medium primary-bg">+ Create Runsheet</a>
                <a href="#" class="btn dropdown-button transparent runsheet-filter" data-activates='runsheet-filter'><img src="/img/filter.svg"></a>
            </h4>
        </div>
        @if(count($runsheet) >= 1)
		    <div class="row">
	        	@foreach ($runsheet as $r)
		        	<div class="col s4">
			        	<div class="card runsheet">
						    <div class="card-content">
						    	<span class="card-title grey-text text-darken-4">
						    		@if(!$r->is_published)
						      		<a href="#" class="dropdown-button material-icons right" data-activates='runsheet-action-{{$r->id}}'><img src="/img/more.svg"></a>
									<ul id='runsheet-action-{{$r->id}}' class='dropdown-content'>
									    <li><a href="#!" data-id="{{$r->id}}">EDIT</a></li>
									    <li><a href="#delRunsheet" data-id="{{$r->id}}" class="modal-trigger delRunsheetBtn" data-bind="{{$r->id}}">DELETE</a></li>
									</ul>
									@endif
									
									@if($r->is_published)
							      		<div class="chip pblsh">Published</div>
						      		@else
							      		<div class="chip inprog">In Progress</div>
						      		@endif
						      	</span>
						      	<div class="runsheet-content">
								    <h6 class="runsheet-title">{{$r->name}}</h6>
								    {{-- <div class="runsheet-group">Marketing Group</div> --}}
								    <div class="runsheet-date">Created {{$r->created_at->diffForHumans()}}</div>
								    <div class="runsheet-info">{{$r->description}}</div>
						      	</div>

					      		@if($r->is_published)
						      	<div class="card-footer center-align">
					      			<a href="#prevRunsheet" class="modal-trigger prevRunsheetBtn" data-id="{{$r->id}}">PREVIEW</a>
					      		@else
					      		<div class="card-footer">
					      			<a href="#pubRunsheet" class="modal-trigger btn-pub" data-id="{{$r->id}}">PUBLISH</a>
					      			<a href="{{ route('admin.runsheets.show', ['runsheet' => $r->id]) }}" class="right">VIEW</a>
					      		@endif
						      	</div>
						    </div>
						</div>
		        	</div>
				@endforeach
	        </div>
        @else
	        <div class="row empty-runsheet">
	        	<img src="/img/empty-runsheet.svg">
	        	<h5>You have not created any Runsheets.</h5>
	        	<p>Organize your legs and create your runsheets now.</p>
	        	<p>Click 'Create Runsheets' at the top right corner.</p>
	        </div>
        @endif
        {{-- 
	        <div class="row runsheet-filtered">
	        	<div class="chip transparent"> Filter </div>
				<div class="chip primary-bg">
				    Published
				    <i class="close material-icons">close</i>
				</div>
	        </div>
        --}}
        
        {{--
	        <div class="row">
	        	<ul class="pagination right">
				    <li class="disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>
				    <li class="active"><a href="#!">1</a></li>
				    <li class="waves-effect"><a href="#!">2</a></li>
				    <li class="waves-effect"><a href="#!">3</a></li>
				    <li class="waves-effect"><a href="#!">4</a></li>
				    <li class="waves-effect"><a href="#!">5</a></li>
				    <li class="waves-effect"><a href="#!"><i class="material-icons">chevron_right</i></a></li>
				  </ul>
	        </div>
        --}}
    </div>
	<ul id='runsheet-filter' class='dropdown-content'>
	    <li><a href="#!">NO FILTER</a></li>
	    <li><a href="#!">PUBLISHED</a></li>
	    <li><a href="#!">IN PROGRESS</a></li>
	</ul>
	<form id="addNewRunsheet" class="modal" action="{{ route('admin.runsheets.store') }}" method="POST" role="form" enctype="multipart/form-data">
	    <div class="modal-content">
	    	<h4>Runsheet Details</h4>
	    	{{ csrf_field() }}
	    	<div class="row">
	            <div class="input-field col s12">
	                <input name="name" type="text" />
	                <label class="active">Name of Runsheet</label>
	            </div>
	        </div>
	    	<div class="row">
	            <div class="input-field col s12">
	                <input name="description" type="text" />
	                <label class="active">Runsheet Description</label>
	            </div>
	        </div>
	    </div>
	    <div class="modal-footer">
	    	<div class="row">
	            <div class="input-field col s12">
	                <button type="submit" id="btn-addRunsheet" href="#" class="modal-action modal-close waves-effect waves-blue-grey btn btn-medium primary-bg">Create Runsheet</button>
	            </div>
            </div>
	    </div>
	</form>
	<div id="prevRunsheet" class="modal">
	    <div class="modal-content">
	    	<h5>Preview</h5>
	    	<div class="leg-content-inner"></div>
	    </div>
	    <div class="modal-footer center-align">
            <a href="#" class="modal-action modal-close waves-effect waves-blue-grey btn btn-medium green">Close Preview</a>
	    </div>
	</div>
	<div id="delRunsheet" class="modal center-align">
	    <div class="modal-content">
	    	<h5>Your about to delete a runsheet.</h5>
	    	<p>Are you sure you want to delete this</p>
	    	<p>If not, click the outside of this box to exit.</p>
	    </div>
	    <div class="modal-footer center-align">
            <a id="btn-delRunsheet" href="#" class="modal-action modal-close waves-effect waves-blue-grey btn btn-medium red">Delete Runsheet</a>
	    </div>
	</div>
	<div id="pubRunsheet" class="modal center-align">
	    <div class="modal-content">
	    	<h5>Your about to publish a runsheet.</h5>
	    	<p>Are you sure you want to publish this</p>
	    	<p>If not, click the outside of this box to exit.</p>
	    </div>
	    <div class="modal-footer center-align">
            <a id="btn-pubRunsheet" href="#" class="modal-action modal-close waves-effect waves-blue-grey btn btn-medium green">Publish Runsheet</a>
	    </div>
	</div>
@endsection

@section('addonjs')
    <script>
        $(document).ready(function(){
            $('.modal').modal();
            var token = '{{ csrf_token()}}';
            $('#btn-addRunsheet').click(function(){
                var toastContent = $('<span>Runsheet Created Successfully</span>');
                Materialize.toast(toastContent, 4000, 'green');
            });
            $('.btn-pub').click(function() {
                var runsheet_id = $(this).data('id');
                $('#pubRunsheet').data("id",runsheet_id);
            });
            $('#btn-pubRunsheet').click(function() {
                $.ajax({
                    url: '/admin/runsheets/' + $('#pubRunsheet').data('id') + '/publish',
                    type: 'POST',
                    data: {
                        _token : token
                    },
                    success: function(data) {
                        $('#pubRunsheet').modal('close');
                        var toastContent = $('<span>Runsheet Successfully Published!</span>');
                        Materialize.toast(toastContent, 2000, 'green',function(){location.reload()});
                    },
                    error: function(error) {
                        console.log(error);
                    }
                });
            });
            $('.prevRunsheetBtn').click(function() {
            	$.ajax({
				   	type: 'GET',
				   	url: '/admin/runsheets/' + $(this).data('id') + '/preview',
				   	beforeSend: function(){
				   		$('#prevRunsheet .leg-content-inner').empty();
				   		$('#prevRunsheet .modal-content').append('<div class="progress">\
						      <div class="indeterminate"></div>\
						  </div>');
				   	},
				   	success: function(res) {
				   		$('#prevRunsheet .progress').remove();
				   		var legs = '',
				   			legs_id= '',
				   			fields = '',
				   			title = '';
				   		$.each(res.data.legs, function(key,leg){
				   			legs_id = "leg"+leg.id;
				   			legs = '<div id="'+legs_id+'"><h5>' + leg.name + '</h5></div>';
				   			$('#prevRunsheet .leg-content-inner').append(legs);

							console.log(leg);
				   			if(!leg.fields.length > 0){
								$('#'+legs_id).append('No Leg Fields Found');
							}else{
							    runsheetPreview(legs_id,leg);
							}
				   		});
				   	},
				   	error: function(data) {
				   		$('#prevRunsheet .progress').remove();
				     	console.log(data);
				   	}
				});
            });
            $('.delRunsheetBtn').click(function() {
                var runsheet_id = $(this).data('id');
                $('#delRunsheet').data("id",runsheet_id);
            });
            $('#delRunsheet #btn-delRunsheet').click(function() {
                $.ajax({
                    url: '/admin/runsheets/' + $('#delRunsheet').data('id'),
                    type: 'POST',
                    data: {
                        _method : 'DELETE',
                        _token : token
                    },
                    success: function(data) {
                        $('#delRunsheet').modal('close');
                        var toastContent = $('<span>Runsheet Successfully Deleted!</span>');
                        Materialize.toast(toastContent, 2000, 'red',function(){location.reload()});
                    },
                    error: function(error) {
                        console.log(error);
                    }
                });
            });
        });
    </script>
@endsection