@extends('layouts.admin')

@section('custom-style')
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css">
	<style type="text/css">
		.runsheet-title p{
			color: #7a7a7a;
		}
		.leg {
			background-color: white;
			padding: 50px 0;
		}
		.empty-leg {
			background-color: transparent;
			color: #7a7a7a;
			border: 3px dashed rgba(122, 122, 122, 0.5);
		}
		.empty-leg h6 {
			font-size: 1.25em;
		}
		.empty-leg p {
			margin: 0;
			font-size: .75em;
		}
		.leg-num {
			margin: 20px 20px 0 20px;
			padding-top: 15px;
			font-size: 20px;
			color: #29ace2;
			background-color: rgba(243, 247, 250, 0.5);
			width: 60px;
			height: 60px;
			text-align: center;
		}
		.leg-title {
			margin-bottom: 5px;
		}
		.leg-content {
			color: #7a7a7a !important;
			font-size: 14px;
		}
		.leg-content span {
			color: #29ace2;
		}
		.leg-info {
			color: #7a7a7a;
			margin-top: 30px !important;
			color: #7a7a7a !important;
		}
		.card .right img {
			width: 20px;
			pointer-events: none;
		}
		.card-footer {
			padding: 20px;
		}
		.card-footer a {
			color: black;
		}
		.modal {
			top: 5%;
			max-height: 80%;
		}
		#delLeg {
			width: 33%;
		}
		#delLeg .modal-footer {
			text-align: center;
		}
	</style>
	<style type="text/css">
		/*select2 adapter*/
		.select2-search--inline ,
		.select2-search__field {
			width: 100% !important;
		}
		.select2-hidden-accessible {
		    top: 60px !important;
		    left: 20px !important;
		}
	</style>
@endsection

@section('content')
	<div class="container">
        <div class="row">
            <div class="col l8 runsheet-title">
            	<h4>{{$runsheet->name}}</h4>
            	<p>{{$runsheet->description}}</p>
            </div>
            <h4 class="col l4 right-align">
                <a href="#addLeg" class="modal-trigger waves-effect waves-light btn btn-medium primary-bg">+ Create Leg</a>
            </h4>
        </div>
        @if(count($leg) >= 1)
        	<div class="row">
	        	@foreach ($leg as $l)
	        		<div class="col s12">
		        		<div class="card horizontal">
					    	<div class="leg-num">0{{$l->sequence}}</div>
					      	<div class="card-stacked">
						        <div class="card-content">
						    	<span class="card-title grey-text text-darken-4">
						      		<a href="#" class="dropdown-button material-icons right" data-activates='leg-action--{{$l->id}}'><img src="/img/more.svg"></a>
									<ul id='leg-action--{{$l->id}}' class='dropdown-content'>
									    <li><a href="#!">EDIT</a></li>
									    <li><a href="#!">DELETE</a></li>
									</ul>
						      	</span>
						        	<h5 class="leg-title">{{$l->name}}</h5>
						        	<p class="leg-info">{{$l->description}}</p>
						        </div>
						      	<div class="card-footer">
						      		<a href="{{ route('admin.leg.show', ['runsheetID' => $runsheet->id, 'legId' => $l->id]) }}" class="right">VIEW</a>
						      	</div>
					      	</div>
					    </div>
		        	</div>
				@endforeach
	        </div>
	    @else
	        <div class="row">
	        	<div class="col s12">
	        		<div class="leg empty-leg center-align">
	        			<h6>Runsheet has no Leg</h6>
	        			<p>Uh oh, Looks like your runsheet won't move as you have no legs yet.</p>
	        			<p>Create now and assign personnel on each task.</p>
	        		</div>
	        	</div>
	        </div>
        @endif
    </div>

	<form id="addLeg" class="modal" action="{{ route('admin.runsheet.leg.store', ['runsheetId' => $runsheet->id]) }}" method="POST" role="form" enctype="multipart/form-data">
    	{{ csrf_field() }}
		
		@if(!count($users) > 1)
            <div class="modal-content">
                <h4>Cannot create a Leg</h4>
                <p>You have insufficient number of users. Fill out atleast another one to create leg.</p>
            </div>
            <div class="modal-footer">
                <a href="{{ url('/admin/users') }}" class="waves-effect waves-light btn btn-medium primary-bg">Add User</a>
            </div>
        @else
	    <div class="modal-content">
	    	<h4>Leg Details</h4>
	    	<div class="row">
	            <div class="input-field col s12">
	                <input name="name" type="text" required="required" />
	                <label class="active">Leg Name</label>
	            </div>
	        </div>
	    	<div class="row">
	            <div class="input-field col s12">
			        <textarea name="description" class="materialize-textarea" required="required"></textarea>
			        <label>Description</label>
	            </div>
	        </div>
	    	<div class="row">
	            <div class="input-field col s12">
					<label>Assigned to:</label>
	            </div>
	            <div class="input-field col s12">
				    <select name="senders[]" class="senders" multiple="multiple" required="required" style="width: 100%">
				    	@foreach($users as $u)
						  	<option value="{{$u->id}}">{{$u->first_name}} {{$u->last_name}}</option>
				    	@endforeach
					</select>
					{{-- <div class="chips chips-initial"></div> --}}
				</div>
	        </div>
	    	<div class="row">
	            <div class="input-field col s12">
				    <select name="reciepients[]" class="reciepients" multiple="multiple" required="required" style="width: 100%">
					  	@foreach($users as $u)
						  	<option value="{{$u->id}}">{{$u->first_name}} {{$u->last_name}}</option>
				    	@endforeach
					</select>
				</div>
	        </div>
	    </div>
	    <div class="modal-footer">
	    	<div class="row">
	            <div class="input-field col s12">
	                <button type="submit" id="btn-add-leg" href="#" class="modal-action waves-effect waves-blue-grey btn btn-medium primary-bg">Add Leg</button>
	                <button type="button" class="modal-action modal-close waves-effect waves-light btn btn-medium red lighten-1">Cancel</button>
	            </div>
            </div>
	    </div>
	    @endif
	</form>

	<div id="delLeg" class="modal center-align">
	    <div class="modal-content">
	    	<h5>Your about to delete a leg.</h5>
	    	<p>Are you sure you want to delete this</p>
	    	<p>If not, click the outside of this box to exit.</p>
	    </div>
	    <div class="modal-footer center-align">
            <a id="btn-add" href="#" class="modal-action modal-close waves-effect waves-blue-grey btn btn-medium red">Delete Leg</a>
	    </div>
	</div>
@endsection

@section('addonjs')
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script>
        $(document).ready(function(){
            $('.modal').modal({
                dismissible: false
            });
            $('select').select2({
	            placeholder: "Choose",
	            allowClear: true,
	            tags: true,
	            createTag: function(params) { /*Disable auto create option when search*/
	                return undefined;
	           }
	        });
        });
    </script>
@endsection