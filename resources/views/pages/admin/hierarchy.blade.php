@extends('layouts.admin')

@section('custom-style')
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/orgchart/2.0.14/css/jquery.orgchart.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
@endsection

@section('content')
	<div class="container">
		<div class="row">
			<div id="orgchart"></div>
		</div>
	</div>
@endsection

@section('addonjs')
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/orgchart/2.0.14/js/jquery.orgchart.min.js"></script>
	<script type="text/javascript">
    $(function() {
    var datascource = {
      'name': 'Super Admin Account',
      'title': 'Super Admin Name',
      'children': [
        { 'name': 'Group A', 'title': 'Group A Lead Name' },
        { 'name': 'Group B', 'title': 'Group B Lead Name',
          'children': [
            { 'name': 'Queso De Bola', 'title': 'Senior cheese' },
            { 'name': 'Hamon De Bola', 'title': 'Senior ham' }
          ]
        },
        { 'name': 'Group C', 'title': 'Group C Lead Name' },
      ]
    };

    var oc = $('#orgchart').orgchart({
      'data' : datascource,
      'nodeContent': 'title',
      'pan': true,
      'zoom': true
    });

    oc.$chartContainer.on('touchmove', function(event) {
      event.preventDefault();
    });

  });
  </script>
@endsection
