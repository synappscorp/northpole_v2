@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row">
        <h4 class="col s12" style="margin-bottom: 0;">{{ $group->name }} <br/></h4>
    </div>
</div>

<nav class="profile-nav row">
    <div class="container">
        <ul class="tabs">
            <li class="tab"><a class="active" href="#infos">INFORMATION</a></li>
            <li class="tab"><a href="#members">MEMBERS</a></li>
            <li class="tab"><a href="#tasks">TASKS</a></li>
        </ul>
    </div>
</nav> 
<div class="row">
    <div class="container">
        <div id="infos" class="col s12">
            <table class="bordered">
                <tr>
                    <td class="title" width="200px">Group Name</td>
                    <td><strong>{{ $group->name }}</strong></td>
                </tr>
                <tr>
                    <td class="title">Group Description</td>
                    <td><strong>{{ $group->description }}</strong></td>
                </tr>
                <tr>
                    <td class="title">Total Users</td>
                    <td>{{ $group->total_members }}</td>
                </tr>
            </table>
        </div>

        <div id="members" class="col s12">
            <table class="col s12 centered modified task-table">
                <thead>
                    <tr>
                        <th width="200px">Username</th>
                        <th>Groups</th>
                        <th>Position</th>
                        <th>Email</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($group->users as $user)
                        <tr class="task">
                            <td>{{ $user->username }}</td>
                            <td>
                                @foreach($user->groups as $group)
                                    <div class="chip">
                                        {{ $group->name }}
                                    </div>
                                @endforeach
                            </td>
                            <td>{{ $user->role }}</td>
                            <td>{{ $user->email }}</td>
                            <td>
                                <a href="#" class="modal-trigger deleteGroupBtn btn-floating tooltipped no-shadow dark-gray-bg actionBtn" data-id="{{ $group->id }}"  data-position="top" data-delay="50" data-tooltip="DELETE">
                                    <i class="material-icons">
                                        <img class="actionSvg" src="/img/trash.svg"/>
                                    </i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

        <div id="tasks">
            <div class="daily-tasks">
                <div class="col s5">
                    <h5>Daily Task List</h5>
                    <span>1 of 3 tasks completed</span>
                    <div class="progress green lighten-3">
                        <div class="determinate green darken-1" style="width: 30%"></div>
                    </div>
                </div>
                <table class="col s12 centered modified task-table">
                    <thead>
                        <tr>
                            <th style="min-width: 200px;">TASK</th>
                            <th>GROUP</th>
                            <th>STATUS</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="task">
                            <td>
                                <span class="name">Task name here</span>
                                <span class="sched">Scheduled for 22 Nov 2017</span>
                            </td>
                            <td>Eclair</td>
                            <td>
                                <span class="indicator late"></span>
                                <span class="status">Late for 2 days</span>
                            </td>
                        </tr>
                        <tr class="task">
                            <td>
                                <span class="name">Task name here</span>
                                <span class="sched">Scheduled for 22 Nov 2017</span>
                            </td>
                            <td>
                                <h6>Task Name Here</h6>
                            </td>
                            <td>
                                <span class="indicator done">
                                    <i class="material-icons">check</i>
                                </span>
                            </td>
                        </tr>
                        <tr class="task">
                            <td>Eclair</td>
                            <td>Eclair</td>
                            <td>
                                <span class="indicator pending"></span>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection