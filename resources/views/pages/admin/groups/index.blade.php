@extends('layouts.admin')

@section('custom-style')
    <style type="text/css">
        .infos {
            margin-left:0;
        }
        .infos .name {
            color: black;
        }
    </style>
@endsection

@section('content')

    <div class="container">
        <div class="row">
            <h4 class="col m4 s12">
                Groups
                <img src="/img/search.svg" class="searchIcon"/>
            </h4>
            <h4 class="col m5 s12 push-m3 right-align">
                <a href="#addNew" class="modal-trigger waves-effect waves-light btn btn-medium primary-bg">+ New Group</a>
                <a class="waves-effect waves-light btn btn-medium green-bg">Import via Excel</a>
            </h4>
        </div>
        <div class="row">
            <div class="col s12">
                <table class="table-responsive modified">
                    <thead>
                        <tr>
                            <th>Name</th>
                            {{-- <th>Members</th> --}}
                            <th>Description</th>
                            <th class="center-align" width="230px">Actions</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($groups as $group)
                            <tr>
                                <td>
                                    <div class="infos">
                                        <h6 class="name">{{ $group->name }}</h6>
                                    </div>
                                </td>
                                {{-- <td>
                                    Member numbers
                                </td> --}}
                                <td>
                                    {{ $group->description }}
                                </td>
                                <td class="center-align">
                                    <a href="{{route('admin.groups.show', ['id' => $group->id])}}" class="btn-floating tooltipped no-shadow dark-gray-bg actionBtn" data-position="top" data-delay="50" data-tooltip="VIEW">
                                        <i class="material-icons">
                                            <img class="actionSvg" src="/img/view.svg"/>
                                        </i>
                                    </a>
                                    <button class="btn-floating tooltipped no-shadow dark-gray-bg actionBtn" data-position="top" data-delay="50" data-tooltip="EDIT">
                                        <i class="material-icons">
                                            <img class="actionSvg" src="/img/edit.svg"/>
                                        </i>
                                    </button>
                                    <a href="#deleteGroup" class="modal-trigger deleteGroupBtn btn-floating tooltipped no-shadow dark-gray-bg actionBtn" data-id="{{ $group->id }}"  data-position="top" data-delay="50" data-tooltip="DELETE">
                                        <i class="material-icons">
                                            <img class="actionSvg" src="/img/trash.svg"/>
                                        </i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <form id="addNew" class="modal" action="{{ route('admin.groups.store') }}" method="POST" role="form" enctype="multipart/form-data">
        <div class="modal-content">
            <h4>Group Details</h4>
            {{ csrf_field() }}
            <div class="row">
                <div class="input-field col s12">
                  <input name="name" id="dept_group" type="text" class="validate">
                  <label for="dept_group">Department / Group Name</label>
                </div>
                <div class="input-field col s12">
                  <textarea name="description" id="description" class="materialize-textarea validate"></textarea>
                  <label for="description">Description</label>
                </div>
            </div>
        </div>
        <div class="modal-footer input-field">
        <button type="submit" id="addGroup" href="#" class="modal-action modal-close waves-effect waves-green btn btn-medium primary-bg">Add Group</button>
        </div>
    </form>

    <div id="deleteGroup" class="modal">
        <div class="modal-content">
          <h4>Delete this group?</h4>
        </div>
        <div class="modal-footer">
          <a id="btn-deleteGroup" href="#!" class="modal-action modal-close waves-effect waves-red btn-flat">Delete</a>
          <a href="#!" class="modal-action modal-close waves-effect btn-flat">Cancel</a>
        </div>
    </div>

@endsection

@section('addonjs')
    <script>
        $(document).ready(function(){
            /* Initializations */
            $('.modal').modal();
            $('select').material_select();
            
            var token = '{{ csrf_token()}}';

            $('#addGroup').click(function(){
                $('#addNew').modal('close');
                var toastContent = $('<span>Group Successfully Added!</span>');
                Materialize.toast(toastContent, 4000, 'green');
             });

            $('.deleteGroupBtn').click(function() {
                var group_id = $(this).data('id');
                $('#deleteGroup').data("id",group_id);
            });
            $('#deleteGroup #btn-deleteGroup').click(function() {
                $.ajax({
                    url: '/admin/groups/' + $('#deleteGroup').data('id'),
                    type: 'POST',
                    data: {
                        _method : 'DELETE',
                        _token : token
                    },
                    success: function(data) {
                        $('#deleteGroup').modal('close');
                        var toastContent = $('<span>Groups Successfully Deleted!</span>');
                        Materialize.toast(toastContent, 2000, 'red',function(){location.reload()});
                    },
                    error: function(error) {
                        console.log(error);
                    }
                });
            });
            
        });
    </script>
@endsection