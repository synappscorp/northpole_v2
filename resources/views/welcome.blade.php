<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="/img/favicon.ico" />

        <!-- CSRF Token -->
        {{-- <meta name="csrf-token" content="{{ csrf_token() }}"> --}}

        <title>@lang('home.title')</title>
        <link href="{{ mix('css/app.css') }}" rel="stylesheet">

        <!-- Styles -->
        <style>
            .main {
                position: relative;
                display: flex;
                align-items: center;
                justify-content: center;
                height: 100vh;
                background-image: url('/img/northpole-bg.jpg');
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .top-right a {
                color: #29ace2;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
                color: white;
            }
        </style>
    </head>
    <body>
        <div class="main">

            @if (Route::has('login'))
                <div class="top-right">
                    @if (Auth::check())
                        <a href="{{ url('/admin') }}" class="btn waves-effect waves-light white">Home</a>
                    @else
                        <a href="{{ url('/login') }}" class="btn waves-effect waves-light white light-blue-text">Login</a>
                    @endif
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    NorthpoleTech
                </div>
            </div>
        </div>
    </body>
</html>
