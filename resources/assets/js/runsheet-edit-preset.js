/*Event for preseted forms*/
$('.leg-field-preseted').click(function(){
	/*Highlight Selected Field*/
	$(this).addClass('leg-field-selected').siblings().removeClass("leg-field-selected");
	/*Change Selected Tab*/
	$('.navtabs ul.tabs').tabs('select_tab', 'leg-setting');
	/*Remove All Field*/
	$('#leg-setting .field').remove();
	/*If Checkbox or Radio*/
	if($('.leg-field-selected').hasClass("field-checkbox") || $('.leg-field-selected').hasClass("field-radio")){

		let field_type = '',
			field_name = '',
			choices = [];

		if($('.leg-field-selected').hasClass("field-checkbox")){
			field_type = 'checkbox';
			field_name = 'Means of Income',
			choices = ['Employment','Entrepreneurship'];
		}else{
			field_type = 'radio';
			field_name = 'Gender',
			choices = ['Male','Female'];
		}

		$('.leg-field-selected').data({
			'field_type': $('.leg-field-selected').data('field_type') ? $('.leg-field-selected').data('field_type') : field_type  ,
			'field_name': $('.leg-field-selected').data('field_name') ? $('.leg-field-selected').data('field_name') : field_name ,
			'choices': $('.leg-field-selected').data('choices') ? $('.leg-field-selected').data('choices'): choices 
		});

		$($('.leg-field-selected').data()).each(function(i,elem) {
			field_type = elem.field_type;
			field_name = elem.field_name;
			choices = elem.choices;
		});


		$('#leg-setting')
			.append('<div class="field field-name-container">\
	    		<label class="label">Field Name</label>\
                <input type="text" class="input" value="'+ field_name +'"/>\
	    	</div>')
			.append('<div class="field choices-container">\
	    		<label class="label">Choices</label>\
	    		<div class="choices">\
                    <input type="text" class="input"/>\
	    		</div>\
	    		<label class="label new-choices">+ Add Another</label>\
	    	</div>');

		/*Events*/
		$('.field-name-container input').keyup(function() {
			$('.leg-field-selected').find('h6').text(this.value);
		  	$('.leg-field-selected').data('field_name',this.value);
		});

		$('.choices').empty();
		$.each($('.leg-field-selected').data('choices'),function(i,value){
			let intfld = '<input type="text" class="input" value="'+ value + '"/>';
			$('.choices').append(intfld);
		});

	    $('.new-choices').click(function(){
    		$('.choices').append('<input type="text" class="input" value="New Item"/>');
    		$('.leg-field-selected').find('.leg-field').append('<p><input type="'+ $('.leg-field-selected').data('field_type') +'"><label>New Item</label></p>');

    		let new_choices = $('.choices input').map(function() {
				    return $(this).val() ? $(this).val() : 'New Item' ;
				}).toArray();
			$('.leg-field-selected').data('choices',new_choices);
    	});
	}
	/*If Date*/
	if($('.leg-field-selected').hasClass("field-date")){
		let field_name = 'Departure Date',
			date_format = 'DD/MM/YY',
			help_note = '';

		$('.leg-field-selected').data({
			'field_name': $('.leg-field-selected').data('field_name') ? $('.leg-field-selected').data('field_name') : field_name  ,
			'date_format': $('.leg-field-selected').data('date_format') ? $('.leg-field-selected').data('date_format') : date_format ,
			'help_note': $('.leg-field-selected').data('help_note') ? $('.leg-field-selected').data('help_note'): help_note
		});

		$($('.leg-field-selected').data()).each(function(i,elem) {
			field_name = elem.field_name;
			date_format = elem.date_format;
			help_note = elem.help_note;
		});

		$('#leg-setting')
			.append('<div class="field field-name-container">\
	    		<label class="label">Field Name</label>\
                <input type="text" class="input" value="'+ field_name +'"/>\
	    	</div>')
			.append('<div class="field date-container">\
	    		<label class="label">Date Format</label>\
                <select>\
			      <option value="DD/MM/YY">DD/MM/YY</option>\
			      <option value="DAY/MONTH/YEAR">DAY/MONTH/YEAR</option>\
			    </select>\
	    	</div>')
			.append('<div class="field helpful-notes-container">\
	    		<label class="label">Helpful Notes</label>\
                <textarea class="materialize-textarea">'+ help_note +'</textarea>\
	    	</div>');

	    /*Events*/
		$('.field-name-container input').keyup(function() {
			$('.leg-field-selected').find('h6').text(this.value);
		  	$('.leg-field-selected').data('field_name',this.value);
		});
		$('.helpful-notes-container textarea').keyup(function() {
		  	$('.leg-field-selected').find('label').text(this.value);
		  	$('.leg-field-selected').data('help_note',this.value);
		});

		$('.leg-field-selected').find('.datepicker').empty();
		if($('select').val() == 'DD/MM/YY'){
			$('.leg-field-selected').data('date_format','DD/MM/YY');
			$('.leg-field-selected').find('.datepicker').append('<span class="col s2">07</span>\
    			<span class="col s2">01</span>\
    			<span class="col s2">18</span>\
    			<span class="col s2 push-l4">\
    				<i class="fa fa-calendar"></i>\
			</span>')
		}else{
			$('.leg-field-selected').data('date_format',this.value);
			$('.leg-field-selected').find('.datepicker').append('<span class="col s2">07</span>\
    			<span class="col s2">January</span>\
    			<span class="col s2">2018</span>\
    			<span class="col s2 push-l4">\
    				<i class="fa fa-calendar"></i>\
			</span>')
		}
	    /*Run Material Select*/
	    $('select').val(date_format).material_select();

		$('select').on('change', function() {
			$('.leg-field-selected').data('date_format',this.value);
			$('.leg-field-selected').find('.datepicker').empty();
	        if($('select').val() == 'DD/MM/YY'){
				$('.leg-field-selected').find('.datepicker').append('<span class="col s2">07</span>\
        			<span class="col s2">01</span>\
        			<span class="col s2">18</span>\
        			<span class="col s2 push-l4">\
        				<i class="fa fa-calendar"></i>\
    			</span>')
			}else{
				$('.leg-field-selected').find('.datepicker').append('<span class="col s2">07</span>\
        			<span class="col s2">January</span>\
        			<span class="col s2">2018</span>\
        			<span class="col s2 push-l4">\
        				<i class="fa fa-calendar"></i>\
    			</span>')
			}
			/*Rerun Material Select*/
	        $('select').material_select();
	    });
	}
	/*If Dropdown*/
	if($('.leg-field-selected').hasClass("field-select")){
		let field_name = 'Manufacturing Status',
			placeholder_name = 'Choose your option',
			choices = ['Option 1','Option 2','Option 3'];

		$('.leg-field-selected').data({
			'field_name': $('.leg-field-selected').data('field_name') ? $('.leg-field-selected').data('field_name') : field_name  ,
			'placeholder_name': $('.leg-field-selected').data('placeholder_name') ? $('.leg-field-selected').data('placeholder_name') : placeholder_name ,
			'choices': $('.leg-field-selected').data('choices') ? $('.leg-field-selected').data('choices'): choices
		});

		$($('.leg-field-selected').data()).each(function(i,elem) {
			field_name = elem.field_name;
			placeholder_name = elem.placeholder_name;
			choices = elem.choices;
		});

		$('#leg-setting')
			.append('<div class="field field-name-container">\
	    		<label class="label">Field Name</label>\
                <input type="text" class="input" value="'+ field_name +'"/>\
	    	</div>')
			.append('<div class="field placeholder-container">\
	    		<label class="label">Placeholder Value</label>\
                <input type="text" class="input" value="'+ placeholder_name +'"/>\
	    	</div>')
			.append('<div class="field choices-container">\
	    		<label class="label">Choices</label>\
	    		<div class="choices">\
                    <input type="text" class="input"/>\
	    		</div>\
	    		<label class="label new-choices">+ Add Another</label>\
	    	</div>');

	    /*Events*/
		$('.field-name-container input').keyup(function() {
			$('.leg-field-selected').find('h6').text(this.value);
		  	$('.leg-field-selected').data('field_name',this.value);
		});

		$('.placeholder-container input').keyup(function() {
			$('select')
				.empty()
				.append('<option value="'+ this.value +'" disabled selected>'+ this.value +'</option>')
				.material_select();
		  	$('.leg-field-selected').data('placeholder_name',this.value);
		});

		$('.choices').empty();
		$.each($('.leg-field-selected').data('choices'),function(i,value){
			let intfld = '<input type="text" class="input" value="'+ value + '"/>';
			$('.choices').append(intfld);
		});
		$('.leg-field-selected').find('p').remove();


	    $('.new-choices').click(function(){
    		$('.choices').append('<input type="text" class="input"/>');

    		let new_choices = $('.choices input').map(function() {
				    return $(this).val() ? $(this).val() : '' ;
				}).toArray();
			$('.leg-field-selected').data('choices',new_choices);
    	});
	}
	/*If Multiline*/
	if($('.leg-field-selected').hasClass("field-multiline") || $('.leg-field-selected').hasClass("field-singleline")){

		let field_name = '',
			placeholder_name = '',
			input_type = '',
			char_allow = '',
			help_note = '';

		if($('.leg-field-selected').hasClass("field-multiline")){
			field_name = 'Report Summary';
			placeholder_name = 'Write Brief Reports',
			input_type = 'Alphanumeric',
			char_allow = '100',
			help_note = 'NOTE: Use 280 Character or Less';
		}else{
			field_name = 'Reviewer\'s Name';
			placeholder_name = 'Enter Complete Name',
			input_type = 'Alphanumeric',
			char_allow = '100',
			help_note = 'Note: Please include suffix if applicable';
		}

		$('.leg-field-selected').data({
			'field_name': $('.leg-field-selected').data('field_name') ? $('.leg-field-selected').data('field_name') : field_name  ,
			'placeholder_name': $('.leg-field-selected').data('placeholder_name') ? $('.leg-field-selected').data('placeholder_name') : placeholder_name ,
			'input_type': $('.leg-field-selected').data('input_type') ? $('.leg-field-selected').data('input_type') : input_type ,
			'char_allow': $('.leg-field-selected').data('char_allow') ? $('.leg-field-selected').data('char_allow') : char_allow ,
			'help_note': $('.leg-field-selected').data('help_note') ? $('.leg-field-selected').data('help_note'): help_note
		});

		$($('.leg-field-selected').data()).each(function(i,elem) {
			field_name = elem.field_name;
			placeholder_name = elem.placeholder_name;
			input_type = elem.input_type;
			char_allow = elem.char_allow;
			help_note = elem.help_note;
		});

		$('#leg-setting')
			.append('<div class="field field-name-container">\
	    		<label class="label">Field Name</label>\
                <input type="text" class="input" value="'+ field_name +'"/>\
	    	</div>')
			.append('<div class="field placeholder-container">\
	    		<label class="label">Placeholder Value</label>\
                <input type="text" class="input" value="'+ placeholder_name +'"/>\
	    	</div>')
			.append('<div class="field input-type-container">\
	    		<label class="label">Input Type</label>\
                <select>\
			      <option value="Alphanumeric">Alphanumeric</option>\
			      <option value="Text Only">Text Only</option>\
			      <option value="Numbers Only">Numbers Only</option>\
			    </select>\
	    	</div>')
	    	.append('<div class="field character-container">\
	    		<label class="label">Character Allowance</label>\
                <input type="number" class="input" min="1" value="'+ char_allow +'"/>\
	    	</div>')
			.append('<div class="field helpful-notes-container">\
	    		<label class="label">Helpful Notes</label>\
                <textarea class="materialize-textarea">'+ help_note +'</textarea>\
	    	</div>');

		/*Run Material Select*/
	    $('select').material_select();

	    /*Events*/
		$('.field-name-container input').keyup(function() {
			$('.leg-field-selected').find('h6').text(this.value);
		  	$('.leg-field-selected').data('field_name',this.value);
		});
		$('.helpful-notes-container textarea').keyup(function() {
		  	$('.leg-field-selected').find('label').text(this.value);
		  	$('.leg-field-selected').data('help_note',this.value);
		});
		if($('select').val() == 'Alphanumeric'){
			$('.leg-field-selected').data('date_format','Alphanumeric');
		}else{
			$('.leg-field-selected').data('date_format',this.value);
		}
		$('.placeholder-container input').keyup(function() {
		  	$('.leg-field-selected').find('textarea').attr('placeholder',this.value);
		  	$('.leg-field-selected').data('placeholder_name',this.value);
		});
	}
	/*If Notes*/
	if($('.leg-field-selected').hasClass("field-notes")){
		let char_allow = '100',
			field_name = 'Please Read',
			note_desc = 'This is the area where you can indicate the instruction or description of whatever could be';

		$('.leg-field-selected').data({
			'char_allow': $('.leg-field-selected').data('char_allow') ? $('.leg-field-selected').data('char_allow') : char_allow ,
			'field_name': $('.leg-field-selected').data('field_name') ? $('.leg-field-selected').data('field_name') : field_name ,
			'note_desc': $('.leg-field-selected').data('note_desc') ? $('.leg-field-selected').data('note_desc'): note_desc
		});

		$($('.leg-field-selected').data()).each(function(i,elem) {
			char_allow = elem.char_allow;
			field_name = elem.field_name;
			note_desc = elem.note_desc;
		});

		$('#leg-setting')
			.append('<div class="field character-container">\
	    		<label class="label">Character Allowance</label>\
                <input type="text" class="input" value="'+ char_allow +'"/>\
	    	</div>')
			.append('<div class="field note-title-container">\
	    		<label class="label">Note Title</label>\
                <input type="text" class="input"  value="'+ field_name +'"/>\
	    	</div>')
			.append('<div class="field notes-container">\
	    		<label class="label">Notes</label>\
                <textarea class="materialize-textarea">'+ note_desc +'</textarea>\
	    	</div>');

		/*Events*/
		$('.character-container input').keyup(function() {
		  	$('.leg-field-selected').data('char_allow',this.value);
		});
		$('.note-title-container input').keyup(function() {
			$('.leg-field-selected').find('h6').text(this.value);
		  	$('.leg-field-selected').data('field_name',this.value);
		});
		$('.notes-container textarea').keyup(function() {
			$('.leg-field-selected').find('p').text(this.value);
		  	$('.leg-field-selected').data('note_desc',this.value);
		});
	}
	/*If Number*/
	if($('.leg-field-selected').hasClass("field-number")){
		let field_name = 'Account Number',
			placeholder_name = '0000-0000',
			help_note = '';

		$('.leg-field-selected').data({
			'field_name': $('.leg-field-selected').data('field_name') ? $('.leg-field-selected').data('field_name') : field_name  ,
			'placeholder_name': $('.leg-field-selected').data('placeholder_name') ? $('.leg-field-selected').data('placeholder_name') : placeholder_name ,
			'help_note': $('.leg-field-selected').data('help_note') ? $('.leg-field-selected').data('help_note'): help_note
		});

		$($('.leg-field-selected').data()).each(function(i,elem) {
			field_name = elem.field_name;
			placeholder_name = elem.placeholder_name;
			help_note = elem.help_note;
		});

		$('#leg-setting')
			.append('<div class="field field-name-container">\
	    		<label class="label">Field Name</label>\
                <input type="text" class="input" value="'+ field_name +'"/>\
	    	</div>')
			.append('<div class="field placeholder-container">\
	    		<label class="label">Placeholder Value</label>\
                <input type="text" class="input" value="'+ placeholder_name +'"/>\
	    	</div>')
			.append('<div class="field helpful-notes-container">\
	    		<label class="label">Helpful Notes</label>\
                <textarea class="materialize-textarea">'+ help_note +'</textarea>\
	    	</div>');

	    /*Events*/
		$('.field-name-container input').keyup(function() {
			$('.leg-field-selected').find('h6').text(this.value);
		  	$('.leg-field-selected').data('field_name',this.value);
		});
		$('.helpful-notes-container textarea').keyup(function() {
		  	$('.leg-field-selected').find('label').text(this.value);
		  	$('.leg-field-selected').data('help_note',this.value);
		});
		$('.placeholder-container input').keyup(function() {
		  	$('.leg-field-selected').find('.numpicker span').text(this.value);
		  	$('.leg-field-selected').data('placeholder_name',this.value);
		});
	}
	/*If Signature*/
	if($('.leg-field-selected').hasClass("field-signature")){
		let field_name = 'Signature Specimen',
			help_note = 'NOTE: SIGN CLEARLY';

		$('.leg-field-selected').data({
			'field_name': $('.leg-field-selected').data('field_name') ? $('.leg-field-selected').data('field_name') : field_name,
			'help_note': $('.leg-field-selected').data('help_note') ? $('.leg-field-selected').data('help_note'): help_note
		});

		$($('.leg-field-selected').data()).each(function(i,elem) {
			field_name = elem.field_name;
			help_note = elem.help_note;
		});

		$('#leg-setting')
			.append('<div class="field field-name-container">\
	    		<label class="label">Field Name</label>\
                <input type="text" class="input" value="'+ field_name +'"/>\
	    	</div>')
			.append('<div class="field helpful-notes-container">\
	    		<label class="label">Helpful Notes</label>\
                <textarea class="materialize-textarea">'+ help_note +'</textarea>\
	    	</div>');

	    /*Events*/
		$('.field-name-container input').keyup(function() {
			$('.leg-field-selected').find('h6').text(this.value);
		  	$('.leg-field-selected').data('field_name',this.value);
		});
		$('.helpful-notes-container textarea').keyup(function() {
		  	$('.leg-field-selected').find('label').text(this.value);
		  	$('.leg-field-selected').data('help_note',this.value);
		});
	}
	/*If Time*/
	if($('.leg-field-selected').hasClass("field-time")){
		let field_name = 'Departure Date',
			time_format = '12-hour';

		$('.leg-field-selected').data({
			'field_name': $('.leg-field-selected').data('field_name') ? $('.leg-field-selected').data('field_name') : field_name ,
			'time_format': $('.leg-field-selected').data('time_format') ? $('.leg-field-selected').data('time_format') : time_format 
		});

		$($('.leg-field-selected').data()).each(function(i,elem) {
			field_name = elem.field_name;
			time_format = elem.time_format;
		});

		$('#leg-setting')
			.append('<div class="field field-name-container">\
	    		<label class="label">Field Name</label>\
                <input type="text" class="input" value="'+ field_name +'"/>\
	    	</div>')
			.append('<div class="field time-container">\
	    		<label class="label">Time Format</label>\
                <select>\
			      <option value="12-hour">12-hour</option>\
			      <option value="24-hour">24-hour</option>\
			    </select>\
	    	</div>');
	    
	    /*Run Material Select*/
	    $('select').val(time_format).material_select();

	    /*Events*/
		$('.field-name-container input').keyup(function() {
			$('.leg-field-selected').find('h6').text(this.value);
		  	$('.leg-field-selected').data('field_name',this.value);
		});
		$('select').on('change', function() {
			$('.leg-field-selected').data('time_format',this.value);
			/*Rerun Material Select*/
	        $('select').material_select();
	    });
	}
	/*If Upload*/
	if($('.leg-field-selected').hasClass("field-upload")){
		let field_name = 'Valid ID',
			help_note = 'NOTE: Please Upload a photo of your primary ID',
			upload_type = 'Single_Image';

		$('.leg-field-selected').data({
			'field_name': $('.leg-field-selected').data('field_name') ? $('.leg-field-selected').data('field_name') : field_name,
			'help_note': $('.leg-field-selected').data('help_note') ? $('.leg-field-selected').data('help_note'): help_note,
			'help_note': $('.leg-field-selected').data('help_note') ? $('.leg-field-selected').data('help_note'): help_note
		});

		$($('.leg-field-selected').data()).each(function(i,elem) {
			field_name = elem.field_name;
			help_note = elem.help_note;
			upload_type = elem.upload_type;
		});

		$('#leg-setting')
			.append('<div class="field field-name-container">\
	    		<label class="label">Field Name</label>\
                <input type="text" class="input" value="'+ field_name +'"/>\
	    	</div>')
			.append('<div class="field helpful-notes-container">\
	    		<label class="label">Helpful Notes</label>\
                <textarea class="materialize-textarea">'+ help_note +'</textarea>\
	    	</div>')
	    	.append('<div class="field upload-type-container">\
	    		<label class="label">Number Uploads</label>\
                <select>\
			      <option value="Single_Image">Single Image</option>\
			      <option value="Multiple_Image">Multiple Image</option>\
			    </select>\
	    	</div>');

	    /*Events*/
		$('.field-name-container input').keyup(function() {
			$('.leg-field-selected').find('h6').text(this.value);
		  	$('.leg-field-selected').data('field_name',this.value);
		});
		$('.helpful-notes-container textarea').keyup(function() {
		  	$('.leg-field-selected').find('label').text(this.value);
		  	$('.leg-field-selected').data('help_note',this.value);
		});

		/*Run Material Select*/
	    $('select').val(upload_type).material_select();

		$('select').on('change', function() {
			$('.leg-field-selected').data('upload_type',this.value);
			/*Rerun Material Select*/
	        $('select').material_select();
	    });
	}
});

/*Remove Field*/
$('#leg-content').find('.leg-field-delete').click(function(){
	$(this).parent().parent().remove();
	/*Remove All Field*/
	$('#leg-setting .field').remove();
	if ( $('#leg-content').children().length <= 2) {
		$('.empty-leg-field').show();
	    $('.empty-leg-field').css("visibility", "visible");
	}
});