window.$ = require('jquery');

function runsheetSave(token,url){
	$('#saveLegField').click(function(){
		$('#leg-setting .empty-leg-field').remove();
		/*Get Leg Fields Element*/
		let leg_fields = $('.leg-content-inner').children(),
			leg_fields_data = [];
		$(leg_fields).each(function(index,element){
			if($(element).hasClass("field-checkbox") || $(element).hasClass("field-radio")){
				let field_type,field_name,choices;
				if($(element).hasClass("field-checkbox")){
					field_type = 'checkbox',
					field_name = 'Means of Income',
					choices = ['Employment','Entrepreneurship'];
				}else{
					field_type = 'radio',
					field_name = 'Gender',
					choices = ['Male','Female'];
				}
				leg_fields_data.push({
					"type" : $(element).data('field_type') || field_type,
					"field_name":  $(element).data('field_name') || field_name,
					"choices": $(element).data('choices')  || choices
				});
			}

			if($(element).hasClass("field-date")){
				leg_fields_data.push({
					"type" : "date",
					"field_name":  $(element).data('field_name') || 'Departure Date',
					"date_format": $(element).data('date_format') || 'DD/MM/YY',
					"help_note": $(element).data('help_note') || ''
				});
			}

			if($(element).hasClass("field-select")){
				leg_fields_data.push({
					"type" : "select",
					"field_name":  $(element).data('field_name') || 'Manufacturing Status',
					"placeholder_name": $(element).data('placeholder_name') || 'Choose your option',
					"choices": $(element).data('choices') || ['Option 1','Option 2','Option 3']
				});
			}

			if($(element).hasClass("field-multiline") || $(element).hasClass("field-singleline")){
					let linetype = '',
					field_name = '',
					placeholder_name = '',
					input_type = '',
					char_allow = '',
					help_note = '';

				if($(element).hasClass("field-multiline")){
					linetype = 'multiline';
					field_name = 'Report Summary';
					placeholder_name = 'Write Brief Reports',
					input_type = 'Alphanumeric',
					char_allow = '100',
					help_note = 'NOTE: Use 280 Character or Less';
				}else{
					linetype = 'singleline';
					field_name = 'Reviewer\'s Name';
					placeholder_name = 'Enter Complete Name',
					input_type = 'Alphanumeric',
					char_allow = '100',
					help_note = 'Note: Please include suffix if applicable';
				}
				leg_fields_data.push({
					"type":  $(element).data('type') || linetype,
					"field_name":  $(element).data('field_name') || field_name,
					"placeholder_name": $(element).data('placeholder_name') || placeholder_name,
					"input_type": $(element).data('input_type') || input_type,
					"char_allow": $(element).data('char_allow') || char_allow,
					"help_note": $(element).data('help_note') || help_note
				});
			}

			if($(element).hasClass("field-notes")){
				leg_fields_data.push({
					"type" : "notes",
					"field_name": $(element).data('field_name') || 'Please Read',
					"char_allow": $(element).data('char_allow') || '100',
					"note_desc": $(element).data('note_desc') || 'This is the area where you can indicate the instruction or description of whatever could be'
				});
			}

			if($(element).hasClass("field-number")){
				leg_fields_data.push({
					"type" : "number",
					"field_name":  $(element).data('field_name') || 'Account Number',
					"placeholder_name": $(element).data('placeholder_name') || '0000-0000',
					"help_note": $(element).data('help_note') || ''
				});
			}

			if($(element).hasClass("field-signature")){
				leg_fields_data.push({
					"type" : "signature",
					"field_name":  $(element).data('field_name') || 'Signature Specimen',
					"help_note": $(element).data('help_note') || 'NOTE: SIGN CLEARLY'
				});
			}

			if($(element).hasClass("field-time")){
				leg_fields_data.push({
					"type" : "time",
					"field_name":  $(element).data('field_name') || 'Departure Date',
					"time_format": $(element).data('time_format') || '12-hour'
				});
			}

			if($(element).hasClass("field-upload")){
				leg_fields_data.push({
					"type" : "upload",
					"field_name":  $(element).data('field_name') || 'Valid ID',
					"help_note": $(element).data('help_note') || 'NOTE: Please Upload a photo of your primary ID',
					"upload_type": $(element).data('upload_type') || 'Single_Image'
				});
			}
			
		});
		if (leg_fields_data.length <= 2) {
		    Materialize.toast('Leg Fields must not be empty', 2000, 'blue');
		}else{
		    $.ajax({
		        url: url,
		        type: 'POST',
		        data: {
		        	data : leg_fields_data,
		            _token : token
		        },
		        beforeSend: function(){
		        	$('.empty-leg-field').hide();
		        	$('.leg-content-inner .field-leg').hide();
					$('.loading-leg-field').show().find('h6').text('Saving Legs');
		        },
		        success: function(data) {
		            Materialize.toast('Fields Successfully Save!', 2000, 'green',function(){
		            	location.reload()
		            });
		            $('.loading-leg-field').show().find('h6').text('Loading Legs');
		        },
		        error: function(data) {
		        	$('.loading-leg-field').hide()
		        	$('.leg-content-inner .field-leg').show();
		            Materialize.toast('Saving Failed. '+ data.responseJSON.message, 4000, 'red');
		        }
		    });
		}
	});
}
window.runsheetSave = runsheetSave;

export default runsheetSave;