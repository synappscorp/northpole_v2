window.$ = require('jquery');

function runsheetPreview(legID,data){
	let elem_cores;
	$(data.fields).each(function(key,elem) {
		/*If Checkbox or Radio*/
		if(elem.type == 'checkbox' || elem.type == 'radio'){
			let choices = '';
			$(elem.choices).each(function(i,el){
				choices += '<p>\
					      <input type="'+elem.type+'" />\
					      <label>'+el+'</label>\
					    </p>'
			});
			elem_cores = $('<div class="field-leg card leg leg-field-preseted field-'+elem.type+'">\
			    <div class="card-stacked">\
			        <div class="card-content row">\
			        	<div class="col s12 leg-field leg-field-'+elem.type+'">\
			        		<h6>'+ elem.title +'</h6>'
			        		+ choices +
					        '</div>\
			        </div>\
		      	</div>\
			</div>').data(elem);
			$('#'+legID).append(elem_cores);
		}
		/*If Date*/
		if(elem.type == 'date'){
			let date_format = elem.date_format || '';

			elem_cores = $('<div class="field-leg card leg leg-field-preseted field-date">\
			    <div class="card-stacked">\
			        <div class="card-content row">\
			        	<div class="col s12 leg-field leg-field-date">\
			        		<h6>'+elem.title+'</h6>\
			        		<div class="row datepicker dp'+ key +'"></div>\
						    <label></label>\
					    </div>\
			        </div>\
		      	</div>\
			</div>').data(elem);

			$('#'+legID).append(elem_cores);

			if(date_format == 'DD/MM/YY'){
				$('#'+legID + '.field-date').data('date_format','DD/MM/YY');
				$('.dp'+ key).append('<span class="col s2">07</span>\
        			<span class="col s2">01</span>\
        			<span class="col s2">18</span>\
        			<span class="col s2 push-l4">\
        				<i class="fa fa-calendar"></i>\
    			</span>')
			}else{
				$('#'+legID  +'.field-date').data('date_format','DAY/MONTH/YEAR');
				$('.dp'+ key).append('<span class="col s2">07</span>\
        			<span class="col s2">January</span>\
        			<span class="col s2">2018</span>\
        			<span class="col s2 push-l4">\
        				<i class="fa fa-calendar"></i>\
    			</span>')
			}
		}
		/*If Dropdown*/
		if(elem.type == 'select'){
			elem_cores = $('<div class="field-leg card leg leg-field-preseted field-select">\
			    <div class="card-stacked">\
			        <div class="card-content row">\
			        	<div class="col s12 leg-field leg-field-select">\
			        		<h6>'+elem.title+'</h6>\
			        		<select>\
							    <option value="Choose your option" disabled selected>Choose your option</option>\
							</select>\
					    </div>\
			        </div>\
		      	</div>\
			</div>').data(elem);
			$('select').material_select();
			$('#'+legID).append(elem_cores);
		}
		/*If Multiline*/
		if(elem.type == 'multiline' || elem.type == 'singleline'){
			let linetype = '';
			let placeholder = elem.placeholder || '';
			if(elem.type == 'multiline'){
				linetype = '<textarea class="materialize-textarea" placeholder="'+placeholder+'"></textarea>'
			}else{
				linetype = '<input type="text" placeholder="'+placeholder+'">';
			}
			elem_cores = $('<div class="field-leg card leg leg-field-preseted field-multiline">\
				<div class="card-stacked">\
			        <div class="card-content row">\
			        	<div class="col s12 leg-field leg-field-multiline">\
			        		<h6>'+elem.title+'</h6>\
			        		'+ linetype +'\
			        		<label>NOTE: Use 280 Character or Less</label>\
					    </div>\
			        </div>\
		      	</div>\
			</div>').data(elem);
			$('#'+legID).append(elem_cores);
		}
		/*If Notes*/
		if(elem.type == 'notes'){
			let note_desc = elem.note_desc || '';
			elem_cores = $('<div class="field-leg card leg leg-field-preseted field-notes">\
				<div class="card-stacked">\
			        <div class="card-content row">\
			        	<div class="col s12 leg-field leg-field-notes">\
			        		<h6'+elem.title+'</h6>\
			        		<p>'+note_desc+'</p>\
					    </div>\
			        </div>\
		      	</div>\
			</div>').data(elem);
			$('#'+legID).append(elem_cores);
		}
		/*If Number*/
		if(elem.type == 'number'){
			let placeholder = elem.placeholder_name || '';
			let help_note = elem.help_note || '';
			elem_cores = $('<div class="field-leg card leg leg-field-preseted field-number">\
				<div class="card-stacked">\
			        <div class="card-content row">\
			        	<div class="col s12 leg-field leg-field-number">\
			        		<h6>'+elem.title+'</h6>\
			        		<div class="row numpicker">\
			        			<span class="col s12">'+placeholder+'</span>\
			        		</div>\
			        		<label>'+help_note+'</label>\
					    </div>\
			        </div>\
		      	</div>\
			</div>').data(elem);
			$('#'+legID).append(elem_cores);
		}
		/*If Signature*/
		if(elem.type == 'signature'){
			let help_note = elem.help_note || '';
			elem_cores = $('<div class="field-leg card leg leg-field-preseted field-signature">\
				<div class="card-stacked">\
			        <div class="card-content row">\
			        	<div class="col s12 leg-field leg-field-signature">\
			        		<h6>'+elem.title+'</h6>\
			        		<textarea class="materialize-textarea"></textarea>\
			        		<label>'+help_note+'</label>\
					    </div>\
			        </div>\
		      	</div>\
			</div>').data(elem);
			$('#'+legID).append(elem_cores);
		}
		/*If Time*/
		if(elem.type == 'time'){
			elem_cores = $('<div class="field-leg card leg leg-field-preseted field-time">\
				<div class="card-stacked">\
			        <div class="card-content row">\
			        	<div class="col s12 leg-field leg-field-time">\
			        		<h6>'+elem.title+'</h6>\
			        		<div class="row timepicker">\
			        			<span class="col s3">00 : 00</span>\
			        			<span class="col s3 push-l6">\
			        				<i class="fa fa-time"></i>\
			        			</span>\
			        		</div>\
					    </div>\
			        </div>\
		      	</div>\
			</div>').data(elem);
			$('#'+legID).append(elem_cores);
		}
		/*If Upload*/
		if(elem.type == 'upload'){
			let help_note = elem.help_note || '';
			elem_cores = $('<div class="field-leg card leg leg-field-preseted field-upload">\
				<div class="card-stacked">\
			        <div class="card-content row">\
			        	<div class="col s12 leg-field leg-field-upload">\
			        		<h6>'+elem.title+'</h6>\
			        		<label>'+help_note+'</label>\
			        		<div class="uploader">\
			        			<div>\
			        				<img src="/img/picture.svg">\
			        				<span>Upload Image</span>\
			        			</div>\
			        		</div>\
					    </div>\
			        </div>\
		      	</div>\
			</div>').data(elem);
			$('#'+legID).append(elem_cores);
		}
	});
};

window.runsheetPreview = runsheetPreview;

export default runsheetPreview;