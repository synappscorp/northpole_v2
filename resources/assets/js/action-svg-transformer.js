/*
 * Replace Action SVG images with inline SVG
 */
$('.actionSvg').each(function(){
    let $img = $(this);
    let imgClass = $img.attr('class');
    let imgURL = $img.attr('src');

    $.get(imgURL, function(data) {
        let $svg = $(data).find('svg');
        if(typeof imgClass !== 'undefined') {
            $svg = $svg.attr('class', imgClass+' replaced-svg');
        }
        $svg = $svg.removeAttr('xmlns:a');
        if(!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
            $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
        }
        $img.replaceWith($svg);
    }, 'xml');
});
$('.actionBtn i').hover(function() {
    let el = $(this);
    let svg = el.find('svg path');
    svg.attr('fill', '#2bbbad');
}, function() {
    let el = $(this);
    let svg = el.find('svg path');
    svg.attr('fill', '#78909c');
});