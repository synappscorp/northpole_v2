let dragula = require('dragula');
$(document).ready(function(){	
	dragula([document.getElementById('leg-fields'), document.getElementById('leg-content')], {
	  	removeOnSpill: function (el, source) {
		    return source !== document.getElementById('leg-fields');
		},
		copy: function (el, source) {
		    return source === document.getElementById('leg-fields');
		},
		accepts: function (el, target) {
			let acceptVal = (target !== document.getElementById('leg-fields'));
		    return acceptVal;
		}
  	})
  	.on('drop', function (el,source) {
		if ($('#leg-setting').children().length != 2) {
		    $('.empty-leg-field').hide();
		}else{
		    $('.empty-leg-field').show();
		}

		/*Process Things*/
		$(el).removeClass('field-leg-source');
		$(el).find('i').remove();
		$(el).find('.field-title').remove();
		$(el).addClass("card leg");

		/*If Checkbox*/
		if($(el).hasClass("field-checkbox")){
			$(el).removeClass("field-checkbox");
			/*Append only of empty*/
			if($(el).children().length == 0 ){
				$(el).append('<div class="card-stacked">\
					      		<span class="leg-field-delete">\
					        		<img src="/img/trash.svg">\
					      		</span>\
						        <div class="card-content row">\
						        	<div class="col s12 leg-field leg-field-checkbox">\
						        		<h6>Means of Income</h6>\
								        <p>\
									      <input type="checkbox" id="checkbox1" />\
									      <label for="checkbox1">Employment</label>\
									    </p>\
								        <p>\
									      <input type="checkbox" id="checkbox2" />\
									      <label for="checkbox2">Entrepreneurship</label>\
									    </p>\
								    </div>\
						        </div>\
					      	</div>');
			}
			$(el).addClass("field-checkbox");
		}
		/*If Date*/
		if($(el).hasClass("field-date")){
			$(el).removeClass("field-date");
			/*Append only of empty*/
			if($(el).children().length == 0 ){
				$(el).append('<div class="card-stacked">\
					      		<span class="leg-field-delete">\
					        		<img src="/img/trash.svg">\
					      		</span>\
						        <div class="card-content row">\
						        	<div class="col s12 leg-field leg-field-date">\
						        		<h6>Departure Date</h6>\
						        		<div class="row datepicker">\
						        			<span class="col s2">07</span>\
						        			<span class="col s2">01</span>\
						        			<span class="col s2">18</span>\
						        			<span class="col s2 push-l4">\
						        				<i class="fa fa-calendar"></i>\
						        			</span>\
						        		</div>\
									    <label></label>\
								    </div>\
						        </div>\
					      	</div>');
			}
			$(el).addClass("field-date");
		}
		/*If Dropdown*/
		if($(el).hasClass("field-select")){
			$(el).removeClass("field-select");
			/*Append only of empty*/
			if($(el).children().length == 0 ){
				$(el).append('<div class="card-stacked">\
					      		<span class="leg-field-delete">\
					        		<img src="/img/trash.svg">\
					      		</span>\
						        <div class="card-content row">\
						        	<div class="col s12 leg-field leg-field-select">\
						        		<h6>Manufacturing Status</h6>\
						        		<select>\
										    <option value="Choose your option" disabled selected>Choose your option</option>\
										</select>\
								    </div>\
						        </div>\
					      	</div>');
			}		
		    /*Run Material Select*/
		    $('select').material_select();
			$(el).addClass("field-select");
		}
		/*If Multiline*/
		if($(el).hasClass("field-multiline")){
			$(el).removeClass("field-multiline");
			/*Append only of empty*/
			if($(el).children().length == 0 ){
				$(el).append('<div class="card-stacked">\
					      		<span class="leg-field-delete">\
					        		<img src="/img/trash.svg">\
					      		</span>\
						        <div class="card-content row">\
						        	<div class="col s12 leg-field leg-field-multiline">\
						        		<h6>Report Summary</h6>\
						        		<textarea class="materialize-textarea" placeholder="Write Brief Reports"></textarea>\
						        		<label>NOTE: Use 280 Character or Less</label>\
								    </div>\
						        </div>\
					      	</div>');
			}
			$(el).addClass("field-multiline");
		}
		/*If Notes*/
		if($(el).hasClass("field-notes")){
			$(el).removeClass("field-notes");
			/*Append only of empty*/
			if($(el).children().length == 0 ){
				$(el).append('<div class="card-stacked">\
					      		<span class="leg-field-delete">\
					        		<img src="/img/trash.svg">\
					      		</span>\
						        <div class="card-content row">\
						        	<div class="col s12 leg-field leg-field-notes">\
						        		<h6>Please Read</h6>\
						        		<p>This is the area where you can indicate the instruction or description of whatever could be</p>\
								    </div>\
						        </div>\
					      	</div>');
			}
			$(el).addClass("field-notes");
		}
		/*If Number*/
		if($(el).hasClass("field-number")){
			$(el).removeClass("field-number");
			/*Append only of empty*/
			if($(el).children().length == 0 ){
				$(el).append('<div class="card-stacked">\
					      		<span class="leg-field-delete">\
					        		<img src="/img/trash.svg">\
					      		</span>\
						        <div class="card-content row">\
						        	<div class="col s12 leg-field leg-field-number">\
						        		<h6>Account Number</h6>\
						        		<div class="row numpicker">\
						        			<span class="col s12">0000-0000</span>\
						        		</div>\
						        		<label></label>\
								    </div>\
						        </div>\
					      	</div>');
			}
			$(el).addClass("field-number");
		}
		/*If Radio*/
		if($(el).hasClass("field-radio")){
			$(el).removeClass("field-radio");
			/*Append only of empty*/
			if($(el).children().length == 0 ){
				$(el).append('<div class="card-stacked">\
					      		<span class="leg-field-delete">\
					        		<img src="/img/trash.svg">\
					      		</span>\
						        <div class="card-content row">\
						        	<div class="col s12 leg-field leg-field-radio">\
						        		<h6>Gender</h6>\
								        <p>\
									      <input type="radio" id="radio1" />\
									      <label for="radio1">Male</label>\
									    </p>\
								        <p>\
									      <input type="radio" id="radio2" />\
									      <label for="radio2">Female</label>\
									    </p>\
								    </div>\
						        </div>\
					      	</div>');
			}
			$(el).addClass("field-radio");
		}
		/*If Signature*/
		if($(el).hasClass("field-signature")){
			$(el).removeClass("field-signature");
			/*Append only of empty*/
			if($(el).children().length == 0 ){
				$(el).append('<div class="card-stacked">\
					      		<span class="leg-field-delete">\
					        		<img src="/img/trash.svg">\
					      		</span>\
						        <div class="card-content row">\
						        	<div class="col s12 leg-field leg-field-signature">\
						        		<h6>Signature Specimen</h6>\
						        		<textarea class="materialize-textarea"></textarea>\
						        		<label>NOTE: SIGN CLEARLY</label>\
								    </div>\
						        </div>\
					      	</div>');
			}
			$(el).addClass("field-signature");
		}
		/*If Singleline*/
		if($(el).hasClass("field-singleline")){
			$(el).removeClass("field-singleline");
			/*Append only of empty*/
			if($(el).children().length == 0 ){
				$(el).append('<div class="card-stacked">\
					      		<span class="leg-field-delete">\
					        		<img src="/img/trash.svg">\
					      		</span>\
						        <div class="card-content row">\
						        	<div class="col s12 leg-field leg-field-singleline">\
						        		<h6>Reviewer\'s Name</h6>\
						        		<input type="text" placeholder="Enter Complete Name">\
						        		<label>Note: Please include suffix if applicable</label>\
								    </div>\
						        </div>\
					      	</div>');
			}
			$(el).addClass("field-singleline");
		}
		/*If Time*/
		if($(el).hasClass("field-time")){
			$(el).removeClass("field-time");
			/*Append only of empty*/
			if($(el).children().length == 0 ){
				$(el).append('<div class="card-stacked">\
					      		<span class="leg-field-delete">\
					        		<img src="/img/trash.svg">\
					      		</span>\
						        <div class="card-content row">\
						        	<div class="col s12 leg-field leg-field-time">\
						        		<h6>Departure Date</h6>\
						        		<div class="row timepicker">\
						        			<span class="col s3">00 : 00</span>\
						        			<span class="col s3 push-l6">\
						        				<i class="fa fa-time"></i>\
						        			</span>\
						        		</div>\
								    </div>\
						        </div>\
					      	</div>');
			}
			$(el).addClass("field-time");
		}
		/*If Upload*/
		if($(el).hasClass("field-upload")){
			$(el).removeClass("field-upload");
			/*Append only of empty*/
			if($(el).children().length == 0 ){
				$(el).append('<div class="card-stacked">\
					      		<span class="leg-field-delete">\
					        		<img src="/img/trash.svg">\
					      		</span>\
						        <div class="card-content row">\
						        	<div class="col s12 leg-field leg-field-upload">\
						        		<h6>Valid ID</h6>\
						        		<label>NOTE: Please Upload a photo of your primary ID</label>\
						        		<div class="uploader">\
						        			<div>\
						        				<img src="/img/picture.svg">\
						        				<span>Upload Image</span>\
						        			</div>\
						        		</div>\
								    </div>\
						        </div>\
					      	</div>');
			}
			$(el).addClass("field-upload");
		}
		/*Remove Field*/
		$(el).find('.leg-field-delete').click(function(){
			$(this).parent().parent().remove();
			/*Remove All Field*/
			$('#leg-setting .field').remove();
			if ( $('#leg-content').children().length <= 2) {
			    $('.empty-leg-field').show();
			    $('.empty-leg-field').css("visibility", "visible");
			}
		});

		$(el).click(function(){
			/*Highlight Selected Field*/
			$(this).addClass('leg-field-selected').siblings().removeClass("leg-field-selected");
			/*Change Selected Tab*/
			$('.navtabs ul.tabs').tabs('select_tab', 'leg-setting');
			/*Remove All Field*/
			$('#leg-setting .field').remove();
			/*If Checkbox or Radio*/
			if($(el).hasClass("field-checkbox") || $(el).hasClass("field-radio")){

				let field_type = '',
					field_name = '',
					choices = [];

				if($(el).hasClass("field-checkbox")){
					field_type = 'checkbox';
					field_name = 'Means of Income',
					choices = ['Employment','Entrepreneurship'];
				}else{
					field_type = 'radio';
					field_name = 'Gender',
					choices = ['Male','Female'];
				}

				$(el).data({
					'field_type': $(el).data('field_type') ? $(el).data('field_type') : field_type  ,
					'field_name': $(el).data('field_name') ? $(el).data('field_name') : field_name ,
					'choices': $(el).data('choices') ? $(el).data('choices'): choices 
				});

				$($(el).data()).each(function(i,elem) {
					field_type = elem.field_type;
					field_name = elem.field_name;
					choices = elem.choices;
				});


				$('#leg-setting')
					.append('<div class="field field-name-container">\
			    		<label class="label">Field Name</label>\
	                    <input type="text" class="input" value="'+ field_name +'"/>\
			    	</div>')
					.append('<div class="field choices-container">\
			    		<label class="label">Choices</label>\
			    		<div class="choices">\
		                    <input type="text" class="input"/>\
			    		</div>\
			    		<label class="label new-choices">+ Add Another</label>\
			    	</div>');

				/*Events*/
				$('.field-name-container input').keyup(function() {
					$(el).find('h6').text(this.value);
				  	$(el).data('field_name',this.value);
				});

	    		$('.choices').empty();
				$.each($(el).data('choices'),function(i,value){
					let intfld = '<input type="text" class="input" value="'+ value + '"/>';
					$('.choices').append(intfld);
				});

			    $('.new-choices').click(function(){
		    		$('.choices').append('<input type="text" class="input" value="New Item"/>');
		    		$('.leg-field-selected').find('.leg-field').append('<p><input type="'+ $(el).data('field_type') +'"><label>New Item</label></p>');

		    		let new_choices = $('.choices input').map(function() {
						    return $(this).val() ? $(this).val() : 'New Item' ;
						}).toArray();
					$(el).data('choices',new_choices);
		    	});
			}
			/*If Date*/
			if($(el).hasClass("field-date")){
				let field_name = 'Departure Date',
					date_format = 'DD/MM/YY',
					help_note = '';

				$(el).data({
					'field_name': $(el).data('field_name') ? $(el).data('field_name') : field_name  ,
					'date_format': $(el).data('date_format') ? $(el).data('date_format') : date_format ,
					'help_note': $(el).data('help_note') ? $(el).data('help_note'): help_note
				});

				$($(el).data()).each(function(i,elem) {
					field_name = elem.field_name;
					date_format = elem.date_format;
					help_note = elem.help_note;
				});

				$('#leg-setting')
					.append('<div class="field field-name-container">\
			    		<label class="label">Field Name</label>\
	                    <input type="text" class="input" value="'+ field_name +'"/>\
			    	</div>')
					.append('<div class="field date-container">\
			    		<label class="label">Date Format</label>\
	                    <select>\
					      <option value="DD/MM/YY">DD/MM/YY</option>\
					      <option value="DAY/MONTH/YEAR">DAY/MONTH/YEAR</option>\
					    </select>\
			    	</div>')
					.append('<div class="field helpful-notes-container">\
			    		<label class="label">Helpful Notes</label>\
	                    <textarea class="materialize-textarea">'+ help_note +'</textarea>\
			    	</div>');

			    /*Events*/
				$('.field-name-container input').keyup(function() {
					$(el).find('h6').text(this.value);
				  	$(el).data('field_name',this.value);
				});
				$('.helpful-notes-container textarea').keyup(function() {
				  	$(el).find('label').text(this.value);
				  	$(el).data('help_note',this.value);
				});

				$(el).find('.datepicker').empty();
				if($('select').val() == 'DD/MM/YY'){
					$(el).data('date_format','DD/MM/YY');
					$(el).find('.datepicker').append('<span class="col s2">07</span>\
	        			<span class="col s2">01</span>\
	        			<span class="col s2">18</span>\
	        			<span class="col s2 push-l4">\
	        				<i class="fa fa-calendar"></i>\
	    			</span>')
				}else{
					$(el).data('date_format',this.value);
					$(el).find('.datepicker').append('<span class="col s2">07</span>\
	        			<span class="col s2">January</span>\
	        			<span class="col s2">2018</span>\
	        			<span class="col s2 push-l4">\
	        				<i class="fa fa-calendar"></i>\
	    			</span>')
				}
			    /*Run Material Select*/
			    $('select').val(date_format).material_select();

				$('select').on('change', function() {
					$(el).data('date_format',this.value);
					$(el).find('.datepicker').empty();
			        if($('select').val() == 'DD/MM/YY'){
						$(el).find('.datepicker').append('<span class="col s2">07</span>\
		        			<span class="col s2">01</span>\
		        			<span class="col s2">18</span>\
		        			<span class="col s2 push-l4">\
		        				<i class="fa fa-calendar"></i>\
		    			</span>')
					}else{
						$(el).find('.datepicker').append('<span class="col s2">07</span>\
		        			<span class="col s2">January</span>\
		        			<span class="col s2">2018</span>\
		        			<span class="col s2 push-l4">\
		        				<i class="fa fa-calendar"></i>\
		    			</span>')
					}
					/*Rerun Material Select*/
			        $('select').material_select();
			    });
			}
			/*If Dropdown*/
			if($(el).hasClass("field-select")){
				let field_name = 'Manufacturing Status',
					placeholder_name = 'Choose your option',
					choices = ['Option 1','Option 2','Option 3'];

				$(el).data({
					'field_name': $(el).data('field_name') ? $(el).data('field_name') : field_name  ,
					'placeholder_name': $(el).data('placeholder_name') ? $(el).data('placeholder_name') : placeholder_name ,
					'choices': $(el).data('choices') ? $(el).data('choices'): choices
				});

				$($(el).data()).each(function(i,elem) {
					field_name = elem.field_name;
					placeholder_name = elem.placeholder_name;
					choices = elem.choices;
				});

				$('#leg-setting')
					.append('<div class="field field-name-container">\
			    		<label class="label">Field Name</label>\
	                    <input type="text" class="input" value="'+ field_name +'"/>\
			    	</div>')
					.append('<div class="field placeholder-container">\
			    		<label class="label">Placeholder Value</label>\
	                    <input type="text" class="input" value="'+ placeholder_name +'"/>\
			    	</div>')
					.append('<div class="field choices-container">\
			    		<label class="label">Choices</label>\
			    		<div class="choices">\
		                    <input type="text" class="input"/>\
			    		</div>\
			    		<label class="label new-choices">+ Add Another</label>\
			    	</div>');

			    /*Events*/
				$('.field-name-container input').keyup(function() {
					$(el).find('h6').text(this.value);
				  	$(el).data('field_name',this.value);
				});

				$('.placeholder-container input').keyup(function() {
					$('select')
						.empty()
						.append('<option value="'+ this.value +'" disabled selected>'+ this.value +'</option>')
						.material_select();
				  	$(el).data('placeholder_name',this.value);
				});

	    		$('.choices').empty();
				$.each($(el).data('choices'),function(i,value){
					let intfld = '<input type="text" class="input" value="'+ value + '"/>';
					$('.choices').append(intfld);
				});
				$(el).find('p').remove();


			    $('.new-choices').click(function(){
		    		$('.choices').append('<input type="text" class="input"/>');

		    		let new_choices = $('.choices input').map(function() {
						    return $(this).val() ? $(this).val() : '' ;
						}).toArray();
					$(el).data('choices',new_choices);
		    	});
			}
			/*If Singleline or Multiline*/
			if($(el).hasClass("field-multiline") || $(el).hasClass("field-singleline")){

				let field_name = '',
					type='',
					placeholder_name = '',
					input_type = '',
					char_allow = '',
					help_note = '';

				if($(el).hasClass("field-multiline")){
					type = 'multiline';
					field_name = 'Report Summary';
					placeholder_name = 'Write Brief Reports',
					input_type = 'Alphanumeric',
					char_allow = '100',
					help_note = 'NOTE: Use 280 Character or Less';
				}else{
					type = 'singleline';
					field_name = 'Reviewer\'s Name';
					placeholder_name = 'Enter Complete Name',
					input_type = 'Alphanumeric',
					char_allow = '100',
					help_note = 'Note: Please include suffix if applicable';
				}

				$(el).data({
					'type': $(el).data('type') ? $(el).data('type') : type  ,
					'field_name': $(el).data('field_name') ? $(el).data('field_name') : field_name  ,
					'placeholder_name': $(el).data('placeholder_name') ? $(el).data('placeholder_name') : placeholder_name ,
					'input_type': $(el).data('input_type') ? $(el).data('input_type') : input_type ,
					'char_allow': $(el).data('char_allow') ? $(el).data('char_allow') : char_allow ,
					'help_note': $(el).data('help_note') ? $(el).data('help_note'): help_note
				});

				$($(el).data()).each(function(i,elem) {
					field_name = elem.field_name;
					placeholder_name = elem.placeholder_name;
					input_type = elem.input_type;
					char_allow = elem.char_allow;
					help_note = elem.help_note;
				});

				$('#leg-setting')
					.append('<div class="field field-name-container">\
			    		<label class="label">Field Name</label>\
	                    <input type="text" class="input" value="'+ field_name +'"/>\
			    	</div>')
					.append('<div class="field placeholder-container">\
			    		<label class="label">Placeholder Value</label>\
	                    <input type="text" class="input" value="'+ placeholder_name +'"/>\
			    	</div>')
					.append('<div class="field input-type-container">\
			    		<label class="label">Input Type</label>\
	                    <select>\
					      <option value="Alphanumeric">Alphanumeric</option>\
					      <option value="Text Only">Text Only</option>\
					      <option value="Numbers Only">Numbers Only</option>\
					    </select>\
			    	</div>')
			    	.append('<div class="field character-container">\
			    		<label class="label">Character Allowance</label>\
	                    <input type="number" class="input" min="1" value="'+ char_allow +'"/>\
			    	</div>')
					.append('<div class="field helpful-notes-container">\
			    		<label class="label">Helpful Notes</label>\
	                    <textarea class="materialize-textarea">'+ help_note +'</textarea>\
			    	</div>');

				/*Run Material Select*/
			    $('select').material_select();

			    /*Events*/
				$('.field-name-container input').keyup(function() {
					$(el).find('h6').text(this.value);
				  	$(el).data('field_name',this.value);
				});
				$('.helpful-notes-container textarea').keyup(function() {
				  	$(el).find('label').text(this.value);
				  	$(el).data('help_note',this.value);
				});
				if($('select').val() == 'Alphanumeric'){
					$(el).data('date_format','Alphanumeric');
				}else{
					$(el).data('date_format',this.value);
				}
				$('.placeholder-container input').keyup(function() {
					if($(el).hasClass("field-multiline")){
						$(el).find('textarea').attr('placeholder',this.value);
					}else{
						$(el).find('input').attr('placeholder',this.value);
					}
				  	$(el).data('placeholder_name',this.value);
				});
			}
			/*If Notes*/
			if($(el).hasClass("field-notes")){
				let char_allow = '100',
					field_name = 'Please Read',
					note_desc = 'This is the area where you can indicate the instruction or description of whatever could be';

				$(el).data({
					'char_allow': $(el).data('char_allow') ? $(el).data('char_allow') : char_allow ,
					'field_name': $(el).data('field_name') ? $(el).data('field_name') : field_name ,
					'note_desc': $(el).data('note_desc') ? $(el).data('note_desc'): note_desc
				});

				$($(el).data()).each(function(i,elem) {
					char_allow = elem.char_allow;
					field_name = elem.field_name;
					note_desc = elem.note_desc;
				});

				$('#leg-setting')
					.append('<div class="field character-container">\
			    		<label class="label">Character Allowance</label>\
	                    <input type="text" class="input" value="'+ char_allow +'"/>\
			    	</div>')
					.append('<div class="field note-title-container">\
			    		<label class="label">Note Title</label>\
	                    <input type="text" class="input"  value="'+ field_name +'"/>\
			    	</div>')
					.append('<div class="field notes-container">\
			    		<label class="label">Notes</label>\
	                    <textarea class="materialize-textarea">'+ note_desc +'</textarea>\
			    	</div>');

				/*Events*/
				$('.character-container input').keyup(function() {
				  	$(el).data('char_allow',this.value);
				});
				$('.note-title-container input').keyup(function() {
					$(el).find('h6').text(this.value);
				  	$(el).data('field_name',this.value);
				});
				$('.notes-container textarea').keyup(function() {
					$(el).find('p').text(this.value);
				  	$(el).data('note_desc',this.value);
				});
			}
			/*If Number*/
			if($(el).hasClass("field-number")){
				let field_name = 'Account Number',
					placeholder_name = '0000-0000',
					help_note = '';

				$(el).data({
					'field_name': $(el).data('field_name') ? $(el).data('field_name') : field_name  ,
					'placeholder_name': $(el).data('placeholder_name') ? $(el).data('placeholder_name') : placeholder_name ,
					'help_note': $(el).data('help_note') ? $(el).data('help_note'): help_note
				});

				$($(el).data()).each(function(i,elem) {
					field_name = elem.field_name;
					placeholder_name = elem.placeholder_name;
					help_note = elem.help_note;
				});

				$('#leg-setting')
					.append('<div class="field field-name-container">\
			    		<label class="label">Field Name</label>\
	                    <input type="text" class="input" value="'+ field_name +'"/>\
			    	</div>')
					.append('<div class="field placeholder-container">\
			    		<label class="label">Placeholder Value</label>\
	                    <input type="text" class="input" value="'+ placeholder_name +'"/>\
			    	</div>')
					.append('<div class="field helpful-notes-container">\
			    		<label class="label">Helpful Notes</label>\
	                    <textarea class="materialize-textarea">'+ help_note +'</textarea>\
			    	</div>');

			    /*Events*/
				$('.field-name-container input').keyup(function() {
					$(el).find('h6').text(this.value);
				  	$(el).data('field_name',this.value);
				});
				$('.helpful-notes-container textarea').keyup(function() {
				  	$(el).find('label').text(this.value);
				  	$(el).data('help_note',this.value);
				});
				$('.placeholder-container input').keyup(function() {
				  	$(el).find('.numpicker span').text(this.value);
				  	$(el).data('placeholder_name',this.value);
				});
			}
			/*If Signature*/
			if($(el).hasClass("field-signature")){
				let field_name = 'Signature Specimen',
					help_note = 'NOTE: SIGN CLEARLY';

				$(el).data({
					'field_name': $(el).data('field_name') ? $(el).data('field_name') : field_name,
					'help_note': $(el).data('help_note') ? $(el).data('help_note'): help_note
				});

				$($(el).data()).each(function(i,elem) {
					field_name = elem.field_name;
					help_note = elem.help_note;
				});

				$('#leg-setting')
					.append('<div class="field field-name-container">\
			    		<label class="label">Field Name</label>\
	                    <input type="text" class="input" value="'+ field_name +'"/>\
			    	</div>')
					.append('<div class="field helpful-notes-container">\
			    		<label class="label">Helpful Notes</label>\
	                    <textarea class="materialize-textarea">'+ help_note +'</textarea>\
			    	</div>');

			    /*Events*/
				$('.field-name-container input').keyup(function() {
					$(el).find('h6').text(this.value);
				  	$(el).data('field_name',this.value);
				});
				$('.helpful-notes-container textarea').keyup(function() {
				  	$(el).find('label').text(this.value);
				  	$(el).data('help_note',this.value);
				});
			}
			/*If Time*/
			if($(el).hasClass("field-time")){
				let field_name = 'Departure Date',
					time_format = '12-hour';

				$(el).data({
					'field_name': $(el).data('field_name') ? $(el).data('field_name') : field_name ,
					'time_format': $(el).data('time_format') ? $(el).data('time_format') : time_format 
				});

				$($(el).data()).each(function(i,elem) {
					field_name = elem.field_name;
					time_format = elem.time_format;
				});

				$('#leg-setting')
					.append('<div class="field field-name-container">\
			    		<label class="label">Field Name</label>\
	                    <input type="text" class="input" value="'+ field_name +'"/>\
			    	</div>')
					.append('<div class="field time-container">\
			    		<label class="label">Time Format</label>\
	                    <select>\
					      <option value="12-hour">12-hour</option>\
					      <option value="24-hour">24-hour</option>\
					    </select>\
			    	</div>');
			    
			    /*Run Material Select*/
			    $('select').val(time_format).material_select();

			    /*Events*/
				$('.field-name-container input').keyup(function() {
					$(el).find('h6').text(this.value);
				  	$(el).data('field_name',this.value);
				});
				$('select').on('change', function() {
					$(el).data('time_format',this.value);
					/*Rerun Material Select*/
			        $('select').material_select();
			    });
			}
			/*If Upload*/
			if($(el).hasClass("field-upload")){
				let field_name = 'Valid ID',
					help_note = 'NOTE: Please Upload a photo of your primary ID',
					upload_type = 'Single_Image';

				$(el).data({
					'field_name': $(el).data('field_name') ? $(el).data('field_name') : field_name,
					'help_note': $(el).data('help_note') ? $(el).data('help_note'): help_note,
					'help_note': $(el).data('help_note') ? $(el).data('help_note'): help_note
				});

				$($(el).data()).each(function(i,elem) {
					field_name = elem.field_name;
					help_note = elem.help_note;
					upload_type = elem.upload_type;
				});

				$('#leg-setting')
					.append('<div class="field field-name-container">\
			    		<label class="label">Field Name</label>\
	                    <input type="text" class="input" value="'+ field_name +'"/>\
			    	</div>')
					.append('<div class="field helpful-notes-container">\
			    		<label class="label">Helpful Notes</label>\
	                    <textarea class="materialize-textarea">'+ help_note +'</textarea>\
			    	</div>')
			    	.append('<div class="field upload-type-container">\
			    		<label class="label">Number Uploads</label>\
	                    <select>\
					      <option value="Single_Image">Single Image</option>\
					      <option value="Multiple_Image">Multiple Image</option>\
					    </select>\
			    	</div>');

			    /*Events*/
				$('.field-name-container input').keyup(function() {
					$(el).find('h6').text(this.value);
				  	$(el).data('field_name',this.value);
				});
				$('.helpful-notes-container textarea').keyup(function() {
				  	$(el).find('label').text(this.value);
				  	$(el).data('help_note',this.value);
				});

				/*Run Material Select*/
			    $('select').val(upload_type).material_select();

				$('select').on('change', function() {
					$(el).data('upload_type',this.value);
					/*Rerun Material Select*/
			        $('select').material_select();
			    });
			}
		});
	})
	.on('remove', function (el) {
		if ( $('#leg-setting').children().length != 2) {
		    // $('.empty-leg-field').css("visibility", "hidden");
		    $('.empty-leg-field').hide();
		}else{
		    // $('.empty-leg-field').css("visibility", "visible");
		    $('.empty-leg-field').show();
		}
	});


	$(document).on('keyup','.choices input',function() {
		let field_type = $('.leg-field-selected').data("field_type");
		let new_choices = $('.choices input').map(function() {
			    return $(this).val() ? $(this).val() : '' ;
			}).toArray();
		$('.leg-field-selected').data('choices',new_choices);

		$('.leg-field-selected').find('p').remove();
		let checkfld = '';
		$.each(new_choices,function(i,val){
    		checkfld = checkfld + '<p><input id="'+ val + '" type="'+ field_type +'"><label for="'+ val + '">'+ val + '</label></p>';
		});

		if($('.leg-field-selected').data("field_type") == 'checkbox'){
			$('.leg-field-selected').find('.leg-field-checkbox').append(checkfld);
		}else{
			$('.leg-field-selected').find('.leg-field-radio').append(checkfld);
		}
    });

});