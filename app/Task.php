<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $table = 'tasks';

    protected $fillable = [
        'table_name',
        'table_name_id',
        'leg_id',
        'runsheet_id',
        'sender_id',
        'recipient_id',
        'due_date',
        'status'
    ];

}
