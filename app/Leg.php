<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Runsheet;
use DB;

class Leg extends Model
{
    protected $table = 'legs';

    protected $fillable = [
        'name',
        'notes',
        'leg_table_name',
        'leg_table_description',
        'sequence',
        'created_by',
        'leg_description',
        'runsheet_id'
    ];

    protected $appends = [
        'runsheet',
        'fields',
        'reciepients'
    ];

    protected function getReciepientsAttribute()
    {
        $reciepients = $this->reciepient()->get()->toArray();

        return array_map(function($reciepient) {
            return [
                'id' => $reciepient['id'],
                'name' => $reciepient['full_name']
            ];
        }, $reciepients);
    }

    public function reciepient()
    {
        return $this->belongsToMany(User::class, 'leg_reciepient');
    }

    public function sender()
    {
        return $this->belongsToMany(User::class, 'leg_sender');
    }

    public function runsheet()
    {
        return $this->belongsToMany(Runsheet::class);
    }

    public function getRunsheetAttribute()
    {
        return Runsheet::find($this->runsheet_id)->toArray();
    }

    public function getFieldsAttribute()
    {
        $fields = DB::table($this->leg_table_description)->select('name', 'title', 'type', 'information')->get();

        return array_map(function($field) {
            $response = [];
            $details = json_decode($field->information, true);

            $response = [
                'type'  => $field->type,
                'title' => $field->title,
                'name'  => $field->name,
            ];

            unset($details['type']);
            unset($details['title']);
            unset($details['name']);

            foreach ($details as $key => $value) {
                $response[$key] = $value;
            }

            return $response;
        },$fields->toArray());
    }
}
