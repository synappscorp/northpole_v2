<?php

namespace App\Northpole\Traits;

use Storage;

trait ImageUploadTrait
{
	/**
	 * Uploads Image
	 *
	 * @param  blob   $image
	 * @param  string $path
	 * @return string
	 */
    protected function uploadImage($image, $path)
    {
        if ($image === null) {
            return '';
        }

        $image = Storage::disk(config('filesystems.default'))->putFile($path, $image, 'public');

        $url = Storage::url($image);

        return $url;
    }
}