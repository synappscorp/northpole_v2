<?php

namespace App\Northpole\Traits;

trait ApiTrait
{
    /**
     * Status Code : default 200
     * @var integer
     */
    protected $status = 200;

    /**
     * Message : default 'Success'
     * @var string
     */
    protected $message = 'Success';

    /**
     * Headers
     * @var array
     */
    protected $headers = [];

    /**
     * Sets the status code
     * @param integer $status
     */
    protected function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * Sets the message
     * @param string $message
     */
    protected function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

    /**
     * Adds a header
     * @param $headers
     */
    protected function addHeaders($headers)
    {
        foreach ($headers as $key => $value) {
            $this->headers[$key] = $value;
        }

        return $this;
    }

    /**
     * Add a single header
     * @param string $key
     * @param string $value
     */
    protected function addHeader($key, $value)
    {
        $this->headers[$key] = $value;
        return $this;
    }

    /**
     * Returns a json response including the setparameters
     * @param  array  $data
     * @return Illuminate/Response
     */
    protected function respond(array $data = [])
    {
        $meta = [
            'status'  => $this->status,
            'message' => $this->message
        ];

        if (!is_null($data)) {
            $meta['data'] = $data;
        }

        return response()->json($meta, $this->status, $this->headers);
    }

    /**
     * Returns a server error
     * @return $this->respond
     */
    protected function respond500()
    {
        $this->setStatus(500);
        $this->setMessage('Server Error');

        return $this->respond();
    }

    /**
     * Returns a 201 status code
     * @param  array  $data
     * @return $this->respond
     */
    protected function respond201(array $data)
    {
        $this->setStatus(201);
        $this->setMessage('Successfully added');

        return $this->respond($data);
    }
}