<?php

namespace App\Northpole\Traits;

use Exception;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\AuthenticationException;

trait ApiExceptionHandlerTrait
{
    protected function getExceptionResponse(Request $request, Exception $e)
    {
        switch (true) {
            case $e instanceof NotFoundHttpException:
                $response = $this->notFoundHttpException($e);
                break;

            case $e instanceof ValidationException:
                $response = $this->validationException($e);
                break;

            case $e instanceof AuthenticationException:
                $response = $this->authException($e);
                break;

            default:
                $response = $this->badRequestException($e);
                break;
        }

        return $response;
    }

    protected function badRequestException($e)
    {
        return response()->json([
            'status'  => 500,
            'message' => $e->getMessage()
        ], 500);
    }

    protected function notFoundHttpException($e)
    {
        return response()->json([
            'status'  => '404',
            'message' => 'Route not found'
        ], 404);
    }

    protected function validationException($e)
    {
        return response()->json([
            'status'  => $e->status,
            'message' => $e->getMessage(),
            'error'   => $e->errors()
        ], $e->status);
    }

    protected function authException($e)
    {
        return response()->json([
            'status'  => 401,
            'message' => $e->getMessage()
        ], 401);
    }
}
