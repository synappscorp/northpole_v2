<?php

namespace App\Northpole\Traits;

use Illuminate\Http\Request;

trait ApiExceptionTrait
{

    /**
     * Check if request url is an api
     *
     * @param  Request $request
     * @return boolean
     */
    protected function isApiCall(Request $request)
    {
        return strpos($request->url(), '/api/') !== false;
    }
}