<?php

if (!function_exists('get_auth_user')) {
    function get_auth_user()
    {
        $user = auth()->user()->toArray();

        return $user;
    }
}

if (!function_exists('upload_image')) {
    function upload_image($image, $path) {
        $storage = new Storage();

        if ($image === null) {
            return '';
        }

        $image = $storage->putFile($path, $image, 'public');
        return $storage->url($image);
    }
}