<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;

class AuthController extends Controller
{
    use AuthenticatesUsers;

    protected $redirectTo = '/admin';

    /**
     * Returns the login view
     * @return View
     */
    public function getLogin()
    {
        return view('pages.login');
    }

    /**
     * Clears the current user session
     * Redirect back to the landing page
     * @return Redirect
     */
    public function logout()
    {
        Auth::logout();
        return redirect(route('/'));
    }

    /**
     * An api logout endpoint
     *
     * @return Response
     */
    public function apiLogout()
    {
        Auth::logout();
        return $this->respond();
    }

    /**
     * Allows the Illuminate\AuthenticateUsers to accept email
     *
     * @param  Request
     * @return array
     */
    protected function credentials(Request $request)
    {
        $field = filter_var($request->get($this->username()), FILTER_VALIDATE_EMAIL)
            ? $this->username()
            : 'username';

        return [
            $field => $request->get($this->username()),
            'password' => $request->password,
        ];
    }
}
