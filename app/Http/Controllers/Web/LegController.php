<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use App\Http\Controllers\Controller;
use App\Leg;
use App\Runsheet;
use DB;

class LegController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($runsheetId, Request $request)
    {
        $runsheet = Runsheet::find($runsheetId);
        $currentLegSequence = count($runsheet->legs()->get());
        $senders = $request->senders;
        $reciepients = $request->reciepients;
        $tableName = str_replace(' ', '_', strtolower($request->name));
        $tableDescription = $tableName . '_description';

        if ($currentLegSequence === null) {
            $sequece = 1;
        } else {
            $sequece = $currentLegSequence + 1;
        }

        $inputData = [
            'name'                  => $request->name,
            'leg_table_name'        => $tableName,
            'leg_table_description' => $tableDescription,
            'notes'                 => $request->notes,
            'sequence'              => $sequece,
            'leg_description'       => $request->description,
            'created_by'            => 1,
            'runsheet_id'           => $runsheet->id,
        ];

        Schema::create($tableName, function(Blueprint $table) {
            $table->increments('id');
            $table->string('geo_tag_latitude')->nullable();
            $table->string('geo_tag_longtitude')->nullable();
            $table->timestamps();
        });

        Schema::create($tableDescription, function(Blueprint $table) {
            $table->string('name')->nullable();
            $table->string('title')->nullable();
            $table->string('type')->nullable();
            $table->string('data_type')->nullable();
            $table->string('validation')->nullable();
            $table->text('information')->nullable();
            $table->timestamps();
        });

        $leg = Leg::create($inputData);

        $leg->sender()->sync($senders);
        $leg->reciepient()->sync($reciepients);

        if ($leg) {
            return redirect(route('admin.leg.show', [
                'runsheetId' => $runsheetId,
                'legId' => $leg->id]
            ));
        }

        return back();
    }

    /**
     * Store a leg description
     *
     * @param  integer  $runsheetId
     * @param  integer  $legId
     * @param  Request  $request
     * @return \Illuminate\Http\Response
     */
    public function legStore($runsheetId, $legId, Request $request)
    {
        $runsheet = Runsheet::find($runsheetId);
        $leg = Leg::find($legId);

        $tableLeg = $leg->leg_table_name;
        $tableDescription = $leg->leg_table_description;
        $fields = $request->data;

        DB::table($tableDescription)->truncate();

        foreach ($fields as $field) {
            $fieldName = str_replace(' ', '_', strtolower($field['field_name']));
            $fieldTitle = $field['field_name'];
            $check = DB::table($tableDescription)->select('*')->where('name', $fieldName)->get();

            if (count($check) >= 1) {
                return $this->setMessage('Fields cannot be duplicated')
                            ->setStatus(422)
                            ->respond();
            }
        }

        foreach($fields as $field) {
            $fieldName = str_replace(' ', '_', strtolower($field['field_name']));
            $fieldTitle = $field['field_name'];
            $check = DB::table($tableDescription)->where('name', $fieldName)->get();

            unset($field['field_name']);

            DB::table($tableDescription)->insert([
                'name'        => $fieldName,
                'title'       => $fieldTitle,
                'type'        => $field['type'],
                'information' => json_encode($field),
            ]);
        }

        $leg = Leg::find($legId);

        return $this->respond();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $leg = Leg::find($id);

        if (!$leg) {
            return redirect(route('admin.runsheets.index'));
        }

        return view('pages.admin.runsheets.leg', compact('leg'));
    }

    /**
     * Returns the list of fields
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function preview($id)
    {
        $leg = Leg::find($id);

        $response = [
            'id'     => $leg->id,
            'name'   => $leg->name,
            'fields' => $leg->fields
        ];

        return $this->respond($response);
    }

}
