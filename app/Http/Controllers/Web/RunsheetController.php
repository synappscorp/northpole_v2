<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Requests\Web\AddRunsheetRequest;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use App\Http\Controllers\Controller;
use App\Runsheet;
use App\Leg;
use App\User;

class RunsheetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $runsheet = Runsheet::all();
        return view('pages.admin.runsheets.index', compact('runsheet'));
    }

    /**
     * Returns a single leg from a runsheet
     * @param  int $runsheetId
     * @param  int $legId
     * @return \Illuminate\Http\Response
     */
    public function leg($runsheetId, $legId)
    {
        $leg = Leg::find($legId);

        if (!$leg) {
            return redirect(route('admin.runsheets.index'));
        }

        return view('pages.admin.runsheets.leg', compact('leg'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($runsheetId, $legId)
    {
        return view('pages.admin.runsheets.view');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddRunsheetRequest $request)
    {
        $tableDescriptionName = str_replace(' ', '_', strtolower($request->name)) . '_description';

        $input = [
            'name'                   => $request->name,
            'description'            => $request->description,
            'created_by'             => 1,
        ];

        $runsheet = Runsheet::create($input);

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $runsheet = Runsheet::find($id);
        $leg = Leg::where('runsheet_id',$id)->get();
        $users = User::get();

        if (!$runsheet) {
            return redirect(route('admin.runsheets.index'));
        }

        return view('pages.admin.runsheets.view' , compact('runsheet','leg','users'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $runsheet = Runsheet::find($id);

        if ($runsheet) {
            Schema::dropIfExists($runsheet->table_description_name);

            $runsheet->delete();
            return $this->respond($runsheet->toArray());
        }

        return $this->repond500();
    }

    /**
     * Publishes the runsheet
     *
     * @param  integer $id
     * @return \Illuminate\Http\Response
     */
    public function published($id)
    {
        $runsheet = Runsheet::find($id);

        if (count($runsheet->legs) == 0) {
            return $this->setMessage('Cannot publish no legs found')
                        ->respond500();
        }

        foreach ($runsheet->legs as $leg) {
            $fields = $leg->fields;
            Schema::table($leg->leg_table_name, function(Blueprint $table) use ($fields) {
                foreach($fields as $field) {
                    $table->string(str_replace(' ', '_', strtolower($field['name'])))->after('id');
                }
            });
        }

        $runsheet->update([
            'is_published' => true
        ]);

        return $this->respond($runsheet->toArray());
    }

    /**
     * Returns the preview of the runsheet
     *
     * @param  integer $id
     * @return \Illuminate\Http\Response
     */
    public function preview($id)
    {
        $result = [];
        $runsheet = Runsheet::find($id);
        $legs = Leg::where('runsheet_id', $id)
                   ->get()
                   ->toArray();

        $result['id'] = $runsheet->id;
        $result['name'] = $runsheet->name;
        $result['legs'] = array_map(function($leg) {
            return [
                'id'     => $leg['id'],
                'name'   => $leg['name'],
                'fields' => $leg['fields'],
            ];
        }, $legs);


        return $this->respond($result);
    }
}
