<?php

/**
 * @todo Clean up codes as in clean up your code mark
 * @todo Repository Pattern
 */

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Requests\Web\AddUserRequest;
use App\Http\Requests\Web\BindUserRequest;
use App\Http\Requests\Common\ImportExcelRequest;
use App\Http\Controllers\Controller;
use App\Jobs\User\SendWelcomeEmail;
use App\User;
use App\Role;
use App\Group;
use Storage;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = $request->has('limit') ? $request->limit : '10';

        $roles = Role::where('name', '!=', 'superadministrator')->get();
        $groups = Group::all();
        $user = get_auth_user();
        $data = User::where('registered_by', $user['id'])
                    ->where('id', '!=', auth()->user()->id)
                    ->paginate($limit);

        return view('pages.admin.users.index', compact([
            'data', 'groups', 'roles'
        ]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::where('name', '!=', 'superadministrator')->get();
        $groups = Group::all();

        return view('pages.admin.users.create', compact([
            'roles', 'groups'
        ]));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddUserRequest $request)
    {
        $temporaryPassword = str_random(6);
        $userImage = $this->uploadImage($request->image, 'user_image');
        $authUser = get_auth_user();
        $groups = $request->groups;

        $input = [
            'first_name'         => ucfirst($request->first_name),
            'middle_name'        => ucfirst($request->middle_name),
            'last_name'          => ucfirst($request->last_name),
            'phone_number'       => $request->phone_number,
            'email'              => $request->email,
            'address'            => $request->address,
            'username'           => $request->username,
            'temporary_password' => $temporaryPassword,
            'password'           => bcrypt($temporaryPassword),
            'account_image'      => $userImage,
            'registered_by'      => $authUser['id'],
        ];

        $user = User::create($input);
        $role = Role::find($request->role);

        $user->attachRole($role);

        foreach($groups as $group) {
            $user->groups()->attach($group);
        }

        SendWelcomeEmail::dispatch($user);

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id)->toArray();

        if (!$user) {
            return redirect()->back()->with([
                'error' => 'Server error please contact provider'
            ]);
        }

        $image = '';

        if (filter_var($user['account_image'], FILTER_VALIDATE_URL)) {
            $image = $user['account_image'];
        } elseif ($user['account_image'] === '') {
            $image = 'https://picsum.photos/200/?random';
        } else {
            $image = asset($user['account_image']);
        }

        $user['account_image'] = $image;

        return view('pages.admin.users.view', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);

        if (!$user) {
            return $this->setStatus(500)
                        ->setMessage('Error')
                        ->respond();
        }

        return view('pages.admin.users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);

        if (!$user) {
            return $this->setStatus(500)
                        ->setMessage('Error')
                        ->respond();
        }

        Storage::delete($user->account_image);

        $user = $user->delete();

        return $this->setMessage('Sucessfully Deleted')
                    ->respond();
    }

    /**
     * Import an excel file
     *
     * @param  ImportExcelRequest
     * @return \Illuminate\Http\Response
     */
    public function import(ImportExcelRequest $request)
    {
        $excel = $request->excel;

    }

    public function searchUser(Request $request)
    {
        $query = $request->has('search') ? $request->search : '';

        if ($query !== '') {
            return User::where('first_name', 'LIKE', '%'.$query.'%')
                       ->orWhere('last_name', 'LIKE', '%'.$query.'%')
                       ->where('user_type', '==', 'user')
                       ->limit(10)
                       ->get()
                       ->pluck('full_name', 'id');
        }

        return User::where('user_type', 'user')
                   ->limit(10)
                   ->get()
                   ->pluck('full_name', 'id');
    }

    public function bind(BindUserRequest $request)
    {
        $user = User::find($request->user);

        $user->groups()->attach($request->group);

        return $this->respond($user);
    }
}
