<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Runsheet;

class AdminController extends Controller
{
    public function index()
    {
    	$runsheet = Runsheet::where('is_published',1)->get();
        return view('pages.admin.index', compact('runsheet'));
    }
}
