<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Task;
use App\User;
use App\Runsheet;
use App\Leg;
use Auth;

class TaskController extends Controller
{
    public function current()
    {
        $dateToday = Carbon::now();
        $user = Auth::user();
        $tasks = Task::where('recipient_id', $user->id)
                    ->where('due_date', '>=', Carbon::today())
                    ->get()
                    ->toArray();

        return $this->respond($this->parseTask($tasks));
    }

    public function completed()
    {
        $user = Auth::user();
        $tasks = Task::where('recipient_id', $user->id)
                    ->where('status', 'done')
                    ->get()
                    ->toArray();

        return $this->respond($this->parseTask($tasks));
    }

    public function delayed()
    {
        $user = Auth::user();
        $tasks = Task::where('recipient_id', $user->id)
                     ->where('due_date', '<', Carbon::today())
                     ->get()
                     ->toArray();

        return $this->respond($this->parseTask($tasks));
    }

    public function getTask($task)
    {
        $task = Task::find($task);
        $runsheet = Runsheet::find($task->runsheet_id)->legs()->get();
        $currentLeg = Leg::find($task->leg_id);
        $nextLeg = Leg::where('runsheet_id', $task->runsheet_id)
                      ->where('sequence', $currentLeg->sequence + 1)
                      ->first();

        foreach($nextLeg->fields as $field) {
            $data[$field['name']] = '';
        }

        $response = [
            'id'          => $nextLeg->id,
            'name'        => $nextLeg->name,
            'description' => $nextLeg->leg_description,
            'fields'      => $nextLeg->fields,
            'reciepient'  => $nextLeg->reciepients,
            'data'        => $data
        ];

        return $this->respond($response);
    }

    private function parseTask($tasks)
    {
        $response = array_map(function($task) {
            $currentLeg = Leg::find($task['leg_id']);
            $nextLeg = Leg::where('sequence', $currentLeg->sequence + 1)
                          ->where('runsheet_id', $currentLeg->runsheet_id)
                          ->first();

            $sender = User::find($task['sender_id']);

            $colorArrays = [
                'fffafa',
                'ffebcd',
                '2f4f4f',
                '708090',
                '87ceeb',
                '8fbc8f',
                '2e8b57',
                'ff69b4',
            ];

            return [
                'id'          => $task['id'],
                'name'        => $nextLeg->name,
                'description' => $nextLeg->description,
                'time_sent'   => Carbon::parse($task['created_at'])->diffForHumans(),
                'sender'      => $sender->full_name,
                'initials'    => strtoupper($sender->name_initials),
                'color'       => $colorArrays[array_rand($colorArrays)]
            ];
        }, $tasks);

        return $response;
    }
}
