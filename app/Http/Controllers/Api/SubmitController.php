<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\DataSubmitRequest;
use App\Leg;
use App\Runsheet;
use App\Task;
use DB;
use Carbon\Carbon;
use Auth;

class SubmitController extends Controller
{
    public function send($leg, DataSubmitRequest $request)
    {
        $data = $request->data;
        $leg = Leg::find($leg);
        $sendTo = $data['send_to'];
        $sender = Auth::user()->id;
        $geoTag = $data['geo_tag'];
        unset($data['send_to']);
        unset($data['geo_tag']);
        $data['geo_tag_latitude'] = $geoTag['latitude'];
        $data['geo_tag_longtitude'] = $geoTag['longtitude'];
        $dueDate = Carbon::now()->addDays(1);

        $submittedData = DB::table($leg->leg_table_name)->insertGetId($data);

        foreach($sendTo as $key => $value) {
            Task::create([
                'table_name'    => $leg->leg_table_name,
                'table_name_id' => $submittedData,
                'leg_id'        => $leg->id,
                'runsheet_id'   => $leg->runsheet_id,
                'sender_id'     => $sender,
                'recipient_id'  => $value,
                'due_date'      => $dueDate,
                'status'        => 'pending'
            ]);
        }

        return $this->respond($data);
    }
}