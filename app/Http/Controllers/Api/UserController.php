<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Runsheet;
use App\Leg;
use Auth;

class UserController extends Controller
{
    public function me(Request $request)
    {
    	return $this->respond(get_auth_user());
    }

    public function runsheet()
    {
        $user = Auth::user();

        if (!is_null($user)) {
            $legs = $user->senderLeg()->get()->toArray();

            $runsheets = array_map(function($leg) {
                return [
                    'id'          => $leg['runsheet']['id'],
                    'name'        => $leg['runsheet']['name'],
                    'description' => $leg['runsheet']['description']
                ];
            }, $legs);

            return $this->respond($runsheets);
        }

        return $this->respond([]);
    }
}
