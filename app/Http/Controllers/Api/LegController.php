<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Runsheet;
use App\Leg;

class LegController extends Controller
{
    public function getLeg($runsheetId)
    {
        $leg = Leg::where('runsheet_id', $runsheetId)
                  ->where('sequence', 1)
                  ->first();

        foreach($leg->fields as $field) {
            $data[$field['name']] = '';
        }

        $response = [
            'id'          => $leg->id,
            'name'        => $leg->name,
            'description' => $leg->leg_description,
            'fields'      => $leg->fields,
            'reciepient'  => $leg->reciepients,
            'data'        => $data
        ];

        return $this->respond($response);
    }
}
