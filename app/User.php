<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Laratrust\Traits\LaratrustUserTrait;
use App\Role;
use App\Leg;
use App\Group;

class User extends Authenticatable
{
    use HasApiTokens;
    use LaratrustUserTrait;
    use Notifiable;

    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'middle_name',
        'last_name',
        'phone_number',
        'email',
        'username',
        'password',
        'temporary_password',
        'account_image',
        'user_type',
        'address',
        'registered_by'
    ];

    /**
     * A list of attributes to be modified
     *
     * @var array
     */
    protected $appends = [
        'full_name',
        'name_initials',
        'role',
    ];

    /**
     * Returns Full Name Attribute
     *
     * @return string
     */
    protected function getFullNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    /**
     * Returns the role in text
     *
     * @return string
     */
    protected function getRoleAttribute()
    {
        return $this->roles()->first()['name'];
    }

    /**
     * Returns the initial of the user
     *
     * @return string
     */
    protected function getNameInitialsAttribute()
    {
        $firstInitial = substr(strtoupper($this->first_name), 0, 1);
        $lastInitial = substr($this->last_name, 0, 1);

        $fullname =  $firstInitial . $lastInitial;

        return $fullname;
    }

    /**
     * Retuns the First name attributes in capital first
     *
     * @param string $value
     */
    public function setFirstNameAttribute($value)
    {
        $this->attributes['first_name'] = ucfirst($value);
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function reciepientLeg()
    {
        return $this->belongsToMany(Leg::class, 'leg_reciepient');
    }

    public function senderLeg()
    {
        return $this->belongsToMany(Leg::class, 'leg_sender');
    }

    public function groups()
    {
        return $this->belongsToMany(Group::class, 'user_group');
    }
}
