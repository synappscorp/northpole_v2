<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Leg;
use App\User;

class Runsheet extends Model
{
    protected $table = 'runsheets';

    protected $fillable = [
        'name',
        'table_name',
        'created_by',
        'is_published',
        'description'
    ];

    public function legs()
    {
        return $this->hasMany(Leg::class);
    }

    public function creator()
    {
        return $this->belongsTo(User::class);
    }
}
