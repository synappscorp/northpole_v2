<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Group extends Model
{
    protected $table = 'groups';

    protected $fillable = [
    	'name',
    	'created_by',
        'description'
    ];

    protected $appends = [
        'total_members'
    ];

    protected function getTotalMembersAttribute()
    {
        return $this->users()->count();
    }

    public function groupHeads()
    {
    	return $this->where('is_head', true)->get();
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'user_group');
    }
}
