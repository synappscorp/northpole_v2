<?php

use Illuminate\Database\Seeder;
use App\Group;

class GroupsDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$groups = [
    		[
    			'name'         => 'Main Group',
                'description'  => 'Sample',
    			'created_by'   => 1
    		],
    		[
    			'name'         => 'Sub Main Group',
                'description'  => 'Sample',
    			'created_by'   => 1
    		]
    	];

        Group::insert($groups);
    }
}
