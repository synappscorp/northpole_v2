<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLegTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('legs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->index();
            $table->string('leg_table_name')->nullable();
            $table->string('leg_table_description')->nullable();
            $table->text('notes')->nullable();
            $table->integer('sequence');
            $table->integer('created_by');
            $table->string('leg_description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leg');
    }
}
