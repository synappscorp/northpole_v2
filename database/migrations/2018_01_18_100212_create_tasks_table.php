<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('table_name');
            $table->integer('table_name_id')->index()->unsigned();
            $table->integer('leg_id')->index()->unsigned();
            $table->integer('runsheet_id')->index()->unsigned();
            $table->integer('sender_id')->index()->unsigned();
            $table->integer('recipient_id')->index()->unsigned();
            $table->date('due_date');
            $table->enum('status', [
                'done',
                'pending',
            ]);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
