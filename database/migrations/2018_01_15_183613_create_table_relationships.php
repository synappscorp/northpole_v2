<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableRelationships extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_group', function (Blueprint $table) {
            $table->integer('user_id')->index()->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('group_id')->index()->unsigned();
            $table->foreign('group_id')->references('id')->on('groups')->onDelete('cascade');
            $table->integer('is_admin')->nullable();
        });

        Schema::create('heirarchy_group', function (Blueprint $table) {
            $table->integer('heirarchy_id')->unsigned()->index();
            $table->foreign('heirarchy_id')->references('id')->on('heirarchy')->onDelete('cascade');
            $table->integer('group_id')->unsigned()->index();
            $table->foreign('group_id')->references('id')->on('groups')->onDelete('cascade');
        });

        Schema::create('leg_reciepient', function(Blueprint $table) {
            $table->integer('leg_id')->index()->unsigned();
            $table->foreign('leg_id')->references('id')->on('legs')->onDelete('cascade');
            $table->integer('user_id')->index()->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });

        Schema::create('leg_sender', function(Blueprint $table){
            $table->integer('leg_id')->index()->unsigned();
            $table->foreign('leg_id')->references('id')->on('legs')->onDelete('cascade');
            $table->integer('user_id')->index()->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });

        Schema::table('legs', function(Blueprint $table) {
            $table->integer('runsheet_id')->index()->unsigned();
            $table->foreign('runsheet_id')->references('id')->on('runsheets');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_group');
        Schema::dropIfExists('heirarchy_group');
        Schema::dropIfExists('leg_user');

        Schema::table('legs', function(Blueprint $table) {
            $table->dropForeign('legs_runsheet_id_foreign');
        });
    }
}
