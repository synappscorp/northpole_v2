# Northpole-tec version 2

A more scalable and maintanable version of the northpoletec

#### Who do I talk to?

* **Ma. Angelica Concepcion : Lead Mobile Dev / Co-Founder**

* **Mark Cornelio : Lead Backend / Co-Founder**

* **Jef Mari : Lead Frontend / Co-Founder**

#### FRONTEND NOTES

##### Development (*Homestead Environment*)

*  `sudo yarn install --no-bin-links --no-optional`
*  Install **cross-env** , **browser-sync** and **acorn** globally
* Check installed global package by running `npm list -g --depth`
* `npm run dev --no-bin-links` or `npm run watch-poll`

##### Production
* Remove 'browser-link' script from body
* `npm run production`

#### API ENDPOINTS

##### API Login

* URL : `oauth/token`
* Method : `POST`
* Parameters : 
    ```json
    {
        "grant_type"    : "password",
        "client_id"     : client-id,
        "client_secret" : client-secret,
        "username"      : username,
        "password"      : password
    }
    ```

* Sample Respons : 

    * Success : 
    ```json
        {
            "token_type": "Bearer",
            "expires_in": 31536000,
            "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6Ijc1YTQ2ZWYwNjg0ODQxNDYwNWU4NzlkZDdjNDk3Y2UwMDY2MjU5MDBmYTNjOWQwZTNmMzk3ZTY4NTQ3NjJjOGM4YjkyNjA5ZDc4OWZlMmYxIn0.eyJhdWQiOiIyIiwianRpIjoiNzVhNDZlZjA2ODQ4NDE0NjA1ZTg3OWRkN2M0OTdjZTAwNjYyNTkwMGZhM2M5ZDBlM2YzOTdlNjg1NDc2MmM4YzhiOTI2MDlkNzg5ZmUyZjEiLCJpYXQiOjE1MDk5OTIyODYsIm5iZiI6MTUwOTk5MjI4NiwiZXhwIjoxNTQxNTI4Mjg2LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.IraSEgdwHgSxrghKwGfCOVY6ZWdMfjFpWTO7tXekZ2SO_Icu7GYXcRLYnm3sMUYIo3F2hZt91D3DM5b59NUgH4syOS54yWs4N-oWUi10WppxE8icCpWyZjgbgzxzhurPWa2AM7yUdBpzs_aiNSbUczwrQhL5Q2Eu5i0Cly56K8XtsknWxxCvrCJiocOHuKA0JUAeC4CpeKJFApH83L0oPAjChVjvdIAOR30TcS4sBoCcSa3Uqp6UlHDN_qtgsbNKArlWVJTx5992kxH_3qkdOeecTHEQUqEU7qdmrSR2gQBTAl2r9NTyoxw67-7Cp_VPtNZ36a5_PEJUzWnfsKq5AP0uob4LjdFlhWmbiFB9TXU_DZOPdVRxvz83LNohv597-zIMe1eLJL0nAkxjtNsTs5K_qtqml95Rd8VpUDXawnAxwiFRS2mQfRi86umKPSfu6ghyoR2pAloM23i2BNHVbhoVqQPfHHuyRsCX7c_ZQSY1X_WCgwSmpusWo2DOE9j6f280wkte3pAQAxROcFTrCx5RHDn6dl-bMLicbvg13LXdB-Sd7iZzJ33c2owqBShtS2mjEIiMxcx1_Z_pHNg3MFXDMEr1ih_VZQ45xGGT6s79CiWDw4EZNmRjhfssFYBRJnnkge1fE6NAzgbqY33eFRc6JNufUvGSjUgw44U_Z2U",
            "refresh_token": "def502001ee2c935411e25e8102761c7eff344990a4eeac6bbdc8d72074fc3ad963890f7ffd39f125a2f7c82eea70f4c32d5c6311e8afc7f026002a82e9181b1b39f605317d65a3403578a035832155aa5c9afc0986db7690d39c580cffa26f321410ef5b6d97d11846a61e4675548fdb1b39aa805460b4dd0c2944e92ff7001888999c678fd8784f0b223bbdf95be485d926f6e1c1b80fcb4124665707b39360a921735a730d4f05bd553f92cf76c491fb023f775264a6d31b9bab6083298b8246a366631ca71e8dbd9712a0d4a49b0d63dc5f414d2e227db83383faa4f458ee845b566b86e79055e10a880679f86cdcc4d88430025fd1f44322541e3d3041dbb937dd244fc0b63eb6a8b3a2851164432f33e179d2bcd48c309e5e36d8992358e0c18cfb5e59258e1d184a0e7d7726f205b2e636597982cf66569fa5642a17f7c83537aae533105f752244bc14def186b8cfa6e962d92619bb298093d5c682620"
        }
    ```
    
    * Error : 
    ```json
        {
            "error": "invalid_credentials",
            "message": "The user credentials were incorrect."
        }
    ```